﻿module Pdb
    open System.Numerics
    open System
    open System.IO

    type AtomData = 
        {
            coordinates: Vector3
            occupancy: float
            tempFactor: float
        }

    type Atom = 
        {
            name: string
            element: string
            serialNumber: int
            dataIndex: int
        }
    
    type Residue = 
        {
            name: string
            sequenceNumber: int
        }

    type Chain = 
        { 
            chainId: char 
        }

    type Model = 
        { 
            modelId: int
        }

    type Protein = 
        {
            pdbId: string
            title: string
        }

    type ProteinElement =
        | AtomData of AtomData
        | Atom of Atom
        | Residue of Residue
        | Chain of Chain
        | Model of Model
        | Protein of Protein

    type ProteinDataContainer<'a> = 
        {
            me: ProteinElement
            data: 'a
        }

    type Layer2<'a> =
        {
            me: 'a
            childs: Layer2<'a> list
        }


    type Layer<'a> =
        | Layer of me: 'a * childs: Layer<'a> list
        | Last of 'a

    //type ILayer<'a, 'b>(me, childs) = 
    //    abstract member me: 'a
    //    abstract member childs: ILayer<'a,'b> list

    //type ResidueLayer(me, atoms) = 
    //    interface ILayer<

    let rec map transformer layer =
        match layer with
            | Last a -> Last (transformer a)
            | Layer (me, childs) -> Layer (transformer me, List.map(fun l -> map transformer l) childs)

    let rec aggregate aggregator layer =
        match layer with
            | Last a -> Last a
            | Layer (me, childs) -> Layer(aggregator(List.map(fun l -> 
                match l with
                | Last a -> a
                | Layer (m, c) -> m ) childs), List.map(aggregate aggregator) childs)
                
    type Pdb = 
        Layer<ProteinElement>

    let parsePdb(sr:StreamReader, id:string) =
        let mutable title = ""
        let mutable t_model : Model option = None
        let mutable t_chain : Chain option = None
        let mutable t_residue : Residue option = None
        let mutable t_model_list = List<Layer<ProteinElement>>.Empty
        let mutable t_chain_list = List<Layer<ProteinElement>>.Empty
        let mutable t_atomdata_list = List<Layer<ProteinElement>>.Empty
        let mutable t_residue_list = List<Layer<ProteinElement>>.Empty
        let mutable t_atom_list = List<Layer<ProteinElement>>.Empty
        while not sr.EndOfStream do
            let line = sr.ReadLine()
            if line.StartsWith("TITLE") then
                title <- title + line.Substring(10);
            if line.StartsWith("MODEL ") then  
                t_model <- Some {
                    modelId = int(line.Substring(6))
                }
                t_atomdata_list <- List<Layer<ProteinElement>>.Empty
            if line.StartsWith("ENDMDL") then
                if t_model_list.Length = 0 then
                    //save residue
                    t_residue_list <- List.append t_residue_list [Layer(Residue(t_residue.Value), t_atom_list)]
                    
                    //save chain
                    t_chain_list <- List.append t_chain_list [Layer(Chain(t_chain.Value), t_residue_list)]
                t_model_list <- Layer(Model(t_model.Value), t_atomdata_list) :: t_model_list
            if line.StartsWith("ATOM  ") then
                if t_model_list.Length = 0 then
                    let seqNum = int(line.[22..25]) //helper
                    
                    //start of chain
                    if t_chain.IsNone then
                        t_chain <- Some { 
                            chainId = line.[21]
                            //index = t_chain_list.Length
                        }

                    //start of residue
                    if t_residue.IsNone then
                        t_residue <- Some { 
                            sequenceNumber = seqNum
                            name = line.[17..19]
                            //index = t_residue_list.Length
                        }

                    //end of residue
                    if t_chain.Value.chainId <> line.[21] || t_residue.Value.sequenceNumber <> seqNum then

                        t_residue_list <- List.append t_residue_list [Layer(Residue(t_residue.Value), t_atom_list)]

                        //create new
                        t_residue <- Some {
                            sequenceNumber = seqNum
                            name = line.[17..19]
                            //index = t_residue_list.Length
                        }
                        t_atom_list <- List<Layer<ProteinElement>>.Empty

                    //end of chain detected
                    if t_chain.Value.chainId <> line.[21] then
                        //save
                        t_chain_list <- List.append t_chain_list [Layer(Chain(t_chain.Value), t_residue_list)]

                        t_chain <- Some {
                            chainId = line.[21]
                            //index = t_chain_list.Length
                        }
                        t_residue_list <- List<Layer<ProteinElement>>.Empty
                    //normal save
                    let t_atom = {
                        serialNumber = int(line.[6..10])
                        name = line.[12..15].Trim()
                        element = line.[76..77].Trim()
                        dataIndex = t_atom_list.Length
                    }
                    t_atom_list <- Last(Atom(t_atom)) :: t_atom_list

                //always save atom data
                let t_atomdata = {
                    occupancy = float(line.[54..59])
                    tempFactor = float(line.[60..65])
                    coordinates = new Vector3(float32 line.[30..37], float32 line.[38..45], float32 line.[46..53])
                }
                t_atomdata_list <- List.append t_atomdata_list [Last(AtomData(t_atomdata))]
        Layer(Protein({
                        title = title.Trim() 
                        pdbId = id
                      }), childs = List.append t_model_list  t_chain_list )


    let dihedral_angle(p0:Vector3,p1:Vector3,p2,p3) = 
        let b0 = -1.0f * (p1 - p0)
        let b1 = p2 - p1
        let b2 = p3 - p2

        let b1 = Vector3.Normalize b1

        let v = b0 - Vector3.Dot(b0, b1) * b1
        let w = b2 - Vector3.Dot(b2, b1) * b1

        let x = Vector3.Dot(v, w)
        let y = Vector3.Dot(Vector3.Cross(b1, v), w)
        Math.Atan2(float(y),float(x)) * (180.0 / Math.PI)
    
    //let pdb_dihedrals(pdb) =
    //    map(fun l -> 
    //        match l with
    //            Last a -> 
    //                match a with
    //                    Atom

    //type ILayer<'T, 'TItem> =
    //    abstract member me: 'T
    //    abstract member childs: Foo<'TItem>

    //type Layer<'T, 'TItem>(me, childs) = 
    //    interface ILayer<'T,'TItem> with
    //        member this.me = me
    //        member this.childs = childs
    
    
    

    //let rec transform (transformer:int->double)(layer:ILayer<int, ILayer<int,_>>):Layer<double, Layer<double, _>> =
    //    match (layer :> ILayer<int,_>).childs with
    //        | [] -> new Layer<double,_>(transformer (layer :> ILayer<int,_>).me, [])
    //        | _ -> new Layer<double,_>(transformer (layer :> ILayer<int,_>).me, ( (List.map((transform transformer)) layer.childs )))


        
    //let rec foo<'a,'b> (bar:'a->'b)(l : 'a list list) = 
        
    //type LastLayer<'T>(me:'T) =
    //    interface ILayer<'T,_> with
    //        member this.me = me
    //        member this.childs = []


    type Class1() = 
        member this.X = "F#"
