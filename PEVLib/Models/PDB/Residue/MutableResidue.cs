﻿//using System.Collections.Generic;

//namespace PEV.Models.Pdb
//{
//    /// <summary>
//    /// A (semi) mutable residue for the complete picture 
//    /// right now this class is not "supported"
//    /// //TODO create guidlines for immutability and implement it
//    /// </summary>
//    internal class MutableResidue : ResidueDto
//    {
//        //public MutableResidue() { }

//        public MutableResidue(ResidueDto r):base(r.sequenceNumber, r.name)
//        {

//        }

//        public int index;
//        public MutableChain chain;
//        public List<Atom> atoms = new List<Atom>();

//        public static implicit operator Residue(MutableResidue r) => 
//            new Residue(r.sequenceNumber, r.name, r.index, r.atoms);

//        public static explicit operator MutableResidue(Residue r) => new MutableResidue(r)
//        {
//            index = r.index,
//            //chain = (MutableChain)r.chain,
//            //atoms = r.atoms.ToList()
//        };
//    }
//}