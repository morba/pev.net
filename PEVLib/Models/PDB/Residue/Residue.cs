﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Runtime.Serialization;
//using System.Collections.Immutable;
//using Newtonsoft.Json;

//namespace PEV.Models.Pdb
//{
//    /// <summary>
//    /// extended representation of a residue for easier calculations
//    /// does not include circular references => serializable
//    /// </summary>
//    public class Residue : ResidueDto
//    {
//        //public Residue() { }

//        [JsonConstructor]
//        public Residue(int sequenceNumber, string name, int index, IEnumerable<Atom> atoms):base(sequenceNumber, name)
//        {
//            this.index = index;
//            //this.atoms = atoms.ToImmutableList();
//        }

//        public Residue(int sequenceNumber, string name, int index, ImmutableList<Atom> atoms) : base(sequenceNumber, name)
//        {
//            this.index = index;
//            //this.atoms = atoms;
//        }

//        /// <summary>
//        /// internal index of residue in <see cref="Chain.residues"/> guaranteed order and continuity 
//        /// (if index 5 exists the so exists index 4,3,2,1,0 and they all come sooner in the pdb file, presumably have smaller <see cref="sequenceNumber"/>)
//        /// TODO kepp it mutable for index reordering ?
//        /// </summary>
//        public readonly int index;

//        ///// <summary>
//        ///// list of atoms belonging to this residue
//        ///// order may not be preserved (it is preserved now but i can not guarantee it for the future)
//        ///// </summary>
//        //public readonly ImmutableList<Atom> atoms;

//        ///// <summary>
//        ///// referece/link to parent chain (useful for finding the next residue)
//        ///// </summary>
//        //[IgnoreDataMember]
//        //public Chain chain;

////#region "helpers"

////        /// <summary>
////        /// internal copy of aminoacid type to automap it to title and name
////        /// </summary>
////        private Residue.AminoAcid? _internal_aa_type;

////        /// <summary>
////        /// type of the amino acid residue as an enum
////        /// the values of the enums should be converted to <see cref="char"/> to get the one letter representation of the aminoacid type when possible.
////        /// also a <see cref="Residue.AminoAcid.Null"/> value exist to describe missing/corrupt information
////        /// </summary>
////        public Residue.AminoAcid AminoAcidType
////        {
////            get
////            {
////                if (_internal_aa_type == null)
////                {
////                    _internal_aa_type = Enum.TryParse(name, true, out Residue.AminoAcid t) ? t : Residue.AminoAcid.Null;
////                }
////                return _internal_aa_type.Value;
////            }
////        }




////#endregion

     
//    }
//}