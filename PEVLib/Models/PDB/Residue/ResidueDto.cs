﻿using Newtonsoft.Json;

namespace PEV.Models.Pdb
{
    /// <summary>
    /// simple representation of a protein aa residue for identification
    /// </summary>
    public class Residue : IPdbTag
    {
        /// <summary>
        /// seqence number in the pdb file (only provide uniqueness and order?)
        /// </summary>
        public int sequenceNumber { get; }

        /// <summary>
        /// name of the aminoacid residue in the pdb. 
        /// here the term name is somewhat confusing type would better describe this field. 
        /// (in a programming example the aminoacid type is the class and this residue is an instance of an aminoacid class with 'unique' atom coordinates and position within the chain)
        /// </summary>
        public string name { get; }

        //public ResidueDto() { }

        [JsonConstructor]
        public Residue(int sequenceNumber, string name)
        {
            this.sequenceNumber = sequenceNumber;
            this.name = name;
        }

    }
}