﻿using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Runtime.Serialization;

namespace PEV.Models.Pdb
{
    public class ResidueLayer : ImmutableLayer<Residue, Atom>
    {
        public ResidueLayer(Residue me, IEnumerable<Atom> atoms):base(me, atoms)
        {

        }

        /// <summary>
        /// assuming that atom names are unique we build a mapping for your convinience, for non unique atom names a __X is appended. Lazily evaluated.
        /// Otherwise please use <see cref="System.Linq.Enumerable.First{TSource}(IEnumerable{TSource}, System.Func{TSource, bool})"/>
        /// as the prefered method to find an atom in a residue
        /// </summary>
        [IgnoreDataMember]
        public IImmutableDictionary<string, Atom> atomMap
        {
            get
            {
                if (_atomMap == null)
                {
                    ImmutableDictionary<string, Atom>.Builder t = ImmutableDictionary.CreateBuilder<string, Atom>();
                    foreach (Atom a in childs)
                    {
                        if (t.ContainsKey(a.name))
                        {
                            t.Add($"{a.name}__{t.Keys.Count(k => k.StartsWith($"{a.name}__"))}", a);
                        }
                        else
                        {
                            t.Add(a.name, a);
                        }
                    }
                    _atomMap = t.ToImmutable();
                }
                return _atomMap;
            }
        }

        private ImmutableDictionary<string, Atom> _atomMap;
    }
}