﻿//using Newtonsoft.Json;
//using System.Runtime.Serialization;

//namespace PEV.Models.Pdb
//{

//    /// <summary>
//    /// pdb model / conformer with only the 'variable part' (eg. atom coordinates)
//    /// </summary>
//    public class Model : ModelDto
//    {
//        //public Model() { }

//        [JsonConstructor]
//        public Model(int pdbModelId, AtomData[] data):base(pdbModelId)
//        {
//            //this.data = data;
//        }

//        ///// <summary>
//        ///// array of atom data for fast access
//        ///// note: leaving it mutable bcs individual atom coordinates can change in future when more calculations are involved.
//        ///// </summary>
//        //public readonly AtomData[] data;

//        ///// <summary>
//        ///// reference to parent protein
//        ///// </summary>
//        //[IgnoreDataMember]
//        //public Protein protein;
//    }
//}
