﻿using Newtonsoft.Json;

namespace PEV.Models.Pdb
{
    public class Model : IPdbTag
    {
        /// <summary>
        /// id of model in the pdb
        /// </summary>
        public readonly int pdbModelId = 1;

        //public ModelDto() { }

        [JsonConstructor]
        public Model(int pdbModelId)
        {
            this.pdbModelId = pdbModelId;
        }
    }
}
