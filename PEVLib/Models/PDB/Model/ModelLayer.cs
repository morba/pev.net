﻿using System.Collections.Generic;

namespace PEV.Models.Pdb
{
    public class ModelLayer : ImmutableLayer<Model, AtomData>
    {
        public ModelLayer(Model me, IEnumerable<AtomData> childs) : base(me, childs)
        {
        }
    }
}
