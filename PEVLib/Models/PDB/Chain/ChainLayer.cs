﻿using System.Collections.Generic;

namespace PEV.Models.Pdb
{
    public class ChainLayer : ImmutableLayer<Chain, ResidueLayer>
    {
        public ChainLayer(Chain me, IEnumerable<ResidueLayer> childs) : base(me, childs)
        {
        }
    }
}