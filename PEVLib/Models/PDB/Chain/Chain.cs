﻿//using System.Linq;
//using System.Collections.Generic;
//using System.Collections.Immutable;
//using System.Runtime.Serialization;
//using Newtonsoft.Json;

//namespace PEV.Models.Pdb
//{

//    /// <summary>
//    /// normal chain representation for basic calculations
//    /// </summary>
//    public class Chain : ChainDto
//    {
//        //public Chain() { }

//        [JsonConstructor]
//        public Chain(IEnumerable<Residue> residues, char pdbChainId = 'A', int index = 0):base(pdbChainId)
//        {
//            //this.residues = residues.ToImmutableList();
//            this.index = index;
//        }

//        public Chain(ImmutableList<Residue> residues, char pdbChainId = 'A', int index = 0):base(pdbChainId)
//        {
//            //this.residues = residues;
//            this.index = index;
//        }

//        ///// <summary>
//        ///// index of chain in the internal representation of pdb <see cref="Protein.chains"/>
//        ///// </summary>
//        //public readonly int index = 0;

//        ///// <summary>
//        ///// array of residues in the chain
//        ///// </summary>
//        ////public readonly ImmutableList<Residue> residues;

//        ///// <summary>
//        ///// referenve to parent protein
//        ///// </summary>
//        //[IgnoreDataMember]
//        //public Protein protein;
//    }
//}