﻿using Newtonsoft.Json;

namespace PEV.Models.Pdb
{
    public class Chain : IPdbTag
    {
        /// <summary>
        /// ID of chain in the PDB
        /// </summary>
        public readonly char pdbChainId = 'A';

        //public ChainDto() { }

        [JsonConstructor]
        public Chain(char pdbChainId)
        {
            this.pdbChainId = pdbChainId;
        }

    }
}