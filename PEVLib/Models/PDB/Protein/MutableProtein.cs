﻿//using System.Collections.Generic;
//using System.Linq;

//namespace PEV.Models.Pdb
//{
//    //TODO create exception hierachy
//    //public class ParsingException : Exception
//    //{
//    //    public string wrong_text { get; set; }
//    //}

//    /// <summary>
//    /// i worked hard on creating an immutable model in c# (which totally made sense in the buisiness logic, and yet json cant handle it...)
//    /// </summary>
//    public class MutableProtein //: ProteinDto
//    {
//        public MutableProtein() { }

//        //public MutableProtein(ProteinDto p):base(p.pdbId, p.title) { }

//        public string pdbId;
//        public string title;
//        public List<MutableChain> chains;
//        public List<MutableModel> models;

//        public static implicit operator Protein(MutableProtein p)=>
//            new Protein(p.pdbId, p.title, p.chains.Select(c=>(Chain)c), p.models.Select(m=>(Model)m));

//        public static explicit operator MutableProtein(Protein p)=> new MutableProtein()
//        {
//            pdbId = p.pdbId,
//            title = p.title,
//            chains = p.chains.Select(c=>(MutableChain)c).ToList(),
//            models = p.models.Select(m=>(MutableModel)m).ToList(),
//        };

//        public MutableProtein fix_uplinking_references()
//        {
//            foreach (Model model in models)
//            {
//                model.protein = this;
//            }
//            foreach (Chain chain in chains)
//            {
//                chain.protein = this;
//                foreach (Residue residue in chain.residues)
//                {
//                    residue.chain = chain;
//                    foreach (Atom atom in residue.atoms)
//                    {
//                        atom.residue = residue;
//                    }
//                }
//            }
//            return this;
//        }

//    }
//}