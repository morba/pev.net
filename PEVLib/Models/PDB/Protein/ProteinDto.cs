﻿using Newtonsoft.Json;

namespace PEV.Models.Pdb
{
    public class Protein : IPdbTag
    {
        public readonly string pdbId;
        public readonly string title;

        //public ProteinDto() { }

        [JsonConstructor]
        public Protein(string pdbId, string title)
        {
            this.pdbId = pdbId;
            this.title = title;
        }
    }
}