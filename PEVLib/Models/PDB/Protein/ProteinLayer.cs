﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;

namespace PEV.Models.Pdb
{
    public class ProteinLayer : ImmutableLayer<Protein, ChainLayer>, IReadonlyLayer<Protein, ModelLayer>
    {
        public ProteinLayer(Protein me, IEnumerable<ChainLayer> childs, IEnumerable<ModelLayer> models) : base(me, childs)
        {
            this.models = models.ToImmutableList();
        }

        public readonly ImmutableList<ModelLayer> models;

        IEnumerable<ModelLayer> IReadonlyLayer<Protein, ModelLayer>.childs => models;
    }
}