﻿//using System;
//using System.Numerics;
//using System.IO;
//using System.Collections.Generic;
//using System.Globalization;
//using System.Collections.Immutable;
//using Newtonsoft.Json;

//namespace PEV.Models.Pdb
//{

//    /// <summary>
//    /// http://www.wwpdb.org/documentation/file-format-content/format33/v3.3.html
//    /// </summary>
//    public class Protein : ProteinDto
//    {
//        //public Protein() { }

//        [JsonConstructor]
//        public Protein(string pdbId, string title, IEnumerable<Model> models):base(pdbId, title)
//        {
//            //this.chains = chains.ToImmutableList();
//            this.models = models.ToImmutableList();
//        }

//        /// <summary>
//        /// a more efficent overload
//        /// </summary>
//        /// <param name="pdbId"></param>
//        /// <param name="title"></param>
//        /// <param name="chains"></param>
//        /// <param name="models"></param>
//        public Protein(string pdbId, string title, ImmutableList<Model> models) : base(pdbId, title)
//        {
//            //this.chains = chains;
//            this.models = models;
//        }

//        //public readonly ImmutableList<Chain> chains;
//        public readonly ImmutableList<Model> models;

//        /// <summary>
//        /// deprecated
//        /// TODO remove these references completly ?
//        /// </summary>
//        public Protein fix_uplinking_references()
//        {
//            foreach (Model model in models)
//            {
//                model.protein = this;
//            }
//            //foreach (Chain chain in chains)
//            //{
//            //    chain.protein = this;
//            //    foreach (Residue residue in chain.residues)
//            //    {
//            //        residue.chain = chain;
//            //        foreach (Atom atom in residue.atoms)
//            //        {
//            //            atom.residue = residue;
//            //        }
//            //    }
//            //}
//            return this;
//        }



        
//    }
//}