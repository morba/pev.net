﻿using Newtonsoft.Json;

namespace PEV.Models.Pdb
{
    /// <summary>
    /// simple representation of an atom for serializtion
    /// </summary>
    public class Atom : IPdbTag
    {
        /// <summary>
        /// Atom name.
        /// </summary>
        public readonly string name;

        /// <summary>
        /// Element symbol, trimmed. As in the PDB file.
        /// </summary>
        public readonly string element;

        /// <summary>
        /// Atom  serial number.
        /// </summary>
        public readonly int serialNumber;

        /// <summary>
        /// index to retrieve data from a model
        /// not in the PDB file, for debug purposes mostly
        /// </summary>
        public readonly int dataIndex;

        //public AtomDto() { }

        [JsonConstructor]
        public Atom(string name, string element, int serialNumber, int dataIndex)
        {
            this.name = name;
            this.element = element;
            this.serialNumber = serialNumber;
            this.dataIndex = dataIndex;
        }
    }
}