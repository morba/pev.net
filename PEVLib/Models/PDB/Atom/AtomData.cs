﻿using System.Numerics;

namespace PEV.Models.Pdb
{
    /// <summary>
    /// part of the atom data that changes beetwen models/conformers
    /// </summary>
    public struct AtomData
    {
        public Vector3 coordinates;
        public float occupancy;
        public float tempFactor;
    }
}