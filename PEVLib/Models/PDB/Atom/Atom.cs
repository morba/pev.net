﻿//using Newtonsoft.Json;
//using System;
//using System.Numerics;
//using System.Runtime.Serialization;

//namespace PEV.Models.Pdb
//{

//    /// <summary>
//    /// http://www.wwpdb.org/documentation/file-format-content/format33/sect9.html#ATOM
//    /// </summary>
//    public class Atom : AtomDto
//    {
//        //public Atom() { }

//        [JsonConstructor]
//        public Atom(string name, string element, int serialNumber, int dataIndex):base(name, element, serialNumber, dataIndex)
//        {
            
//        }

//        /// <summary>
//        /// typed chemical element enum
//        /// element number should be its atomic number in the periodic table. 
//        /// to be completed when necessery
//        /// </summary>
//        public enum Element
//        {
//            /// <summary>
//            /// special case for Not Available 
//            /// </summary>
//            Null = 0,
//            C = 6,
//            H = 1,
//            N = 7,
//            O = 8,
//            S = 16
//        }

//        /// <summary>
//        /// try to convert raw element to enum
//        /// return <see cref="Element.Null"/> if TryParse failed
//        /// </summary>
//        public Element elementType
//        {
//            get => Enum.TryParse(element, out Element t) ? t : Element.Null;
//        }

//        ///// <summary>
//        ///// reference to the Residue containing this atom
//        ///// </summary>
//        //[IgnoreDataMember]
//        //public Residue residue;

//        ///// <summary>
//        ///// get the atomic coordinates for the given model
//        ///// </summary>
//        ///// <param name="model_index">index of model in <see cref="Protein.models"/></param>
//        ///// <returns><see cref="Vector3"/> of coordinates</returns>
//        //public Vector3 get_coordinates(int model_index = 0) => residue.chain.protein.models[model_index].data[dataIndex].coordinates;

//        ///// <summary>
//        ///// get all the model specific data about this atom
//        ///// (aka. coordinates, occupancy, tempFactor)
//        ///// </summary>
//        ///// <param name="model_index">index of model in <see cref="Protein.models"/></param>
//        ///// <returns><see cref="Atom.AtomData"/> structure</returns>
//        //public AtomData get_data(int model_index = 0) => residue.chain.protein.models[model_index].data[dataIndex];
//    }
//}