﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using PEV.Models.Pdb;

namespace PEV.Models.Ensemble
{

    public class ConformerContainerDto<TData>
    {
        public TData data { get; set; }
        public Model model { get; set; }

    }

    public class ConformerContainerLayer<T> : Layer<ConformerContainerDto<T>, ChainContainerLayer<T>>
    {

    }

    //public class ConformerContainerDto<TResidue, TResidueData, TResidueCont, TChain, TChainData, TChainCont, TModel, TModelData> 
    //    : ConformerContainerDto<TModelData, TModel>
    //    where TResidue : ResidueDto
    //    where TResidueCont : ResidueContainerDto<TResidueData, TResidue>
    //    where TChain : ChainDto
    //    where TChainCont : ChainContainerDto<TResidue, TResidueData, TResidueCont, TChain, TChainData>
    //    where TModel : ModelDto
    //{
    //    public List<TChainCont> chains { get; set; } = new List<TChainCont>();
    //}

    ///// <summary>
    ///// your polimorfism sucks......
    ///// </summary>
    ///// <typeparam name="T"></typeparam>
    ///// <typeparam name="TModel"></typeparam>
    ///// <typeparam name="TChainCont"></typeparam>
    ///// <typeparam name="TChain"></typeparam>
    ///// <typeparam name="TResidueCont"></typeparam>
    ///// <typeparam name="TResidue"></typeparam>
    //public class ConformerContainerDto<T, TModel, TChainCont, TChain, TResidueCont, TResidue> 
    //    : ConformerContainerDto<TResidue, T, TResidueCont, TChain, T, TChainCont, TModel, T>
    //    where TModel : ModelDto
    //    where TChain : ChainDto
    //    where TResidue : ResidueDto
    //    where TResidueCont : ResidueContainerDto<T, TResidue>
    //    where TChainCont : ChainContainerDto<T, TChain, TResidue, TResidueCont>
    //{
        
    //}

    //public class ConformerContainerDto<T> : ConformerContainerDto<T, ModelDto, ChainContainerDto<T>, ChainDto, ResidueContainerDto<T>, ResidueDto>
    //{

    //}

    //public class ConformerContainer<T> : ConformerContainerDto<T, Model, ChainContainer<T>, Chain, ResidueContainer<T>, Residue>
    //{
        
    //}

    
}