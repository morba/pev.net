﻿using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using PEV.Models.Pdb;

namespace PEV.Models.Ensemble
{
    public class ChainContainerDto<TData>
    {
        public TData data { get; set; }
        public Chain chain { get; set; }

    }

    public class ChainContainerLayer<T> : Layer<ChainContainerDto<T>, ResidueContainerDto<T>>
    {

    }

    //public class ChainContainerLayer<TData> : Layer<ChainContainerDto<TData, ChainDto>, ResidueContainerDto<TData>>

    //public class ChainContainerDto<TResidue, TResidueData, TResidueCont, TChain, TChainData> : ChainContainerDto<TChainData, TChain>
    //    where TChain : ChainDto
    //    where TResidue : ResidueDto
    //    where TResidueCont : ResidueContainerDto<TResidueData, TResidue>
    //{
    //    public List<TResidueCont> residues { get; set; } = new List<TResidueCont>();
    //}

    ////TODO chain container with atom data type parameter

    //public class ChainContainerDto<T, TChain, TResidue, TResidueCont> : ChainContainerDto<TResidue, T, TResidueCont, TChain, T>
    //    where TChain : ChainDto
    //    where TResidue : ResidueDto
    //    where TResidueCont : ResidueContainerDto<T, TResidue>
    //{
        
    //}

    ///// <summary>
    ///// for the currenct calculation model. //TODO refactor with layer
    ///// </summary>
    ///// <typeparam name="T"></typeparam>
    //public class ChainContainerDto<T> : ChainContainerDto<T, ChainDto, ResidueDto, ResidueContainerDto<T>>
    //{

    //}


    //public class ChainContainer<T> : ChainContainerDto<T, Chain, Residue, ResidueContainer<T>>
    //{

    //}

  

}