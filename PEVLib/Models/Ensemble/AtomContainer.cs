﻿using System;
using PEV.Models.Pdb;

namespace PEV.Models.Ensemble
{


    /// <summary>
    /// not implemnented
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="TAtom"></typeparam>
    public class AtomContainerDto<T>
    {
        public T data;
        public Atom atom;
    }

    //public class AtomContainer<T> : AtomContainerDto<T, Atom>
    //{
    //    public AtomContainer()
    //    {
    //        throw new NotImplementedException();
    //    }
    //}
}