﻿using System;
using System.Collections;
using System.Collections.Generic;

using PEV.Models.Pdb;
using System.Collections.Immutable;
using System.Linq;

namespace PEV.Models.Ensemble
{
    //public class EnsembleContainer<TEnsembleData, TConformerData, TChainData, TResidueData>
    //{
    //    [IgnoreDataMember]
    //    public Protein pdb;
    //    public TEnsembleData data;
    //    public List<ConformerContainer<TConformerData, TChainData, TResidueData>> conformers = new List<ConformerContainer<TConformerData, TChainData, TResidueData>>();
    //}

    public class EnsembleContainerDto<TData>
    {
        public TData data;
        public Protein pdb;
    }

    public class EnsembleContainerLayer<T> : Layer<EnsembleContainerDto<T>, ConformerContainerLayer<T>>
    {

    }

    /// <summary>
    /// same data with better names. so ppl will understand the json
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class EnsembleDto<T>
    {
        public T data;
        public Protein pdb;
        public ImmutableList<ModelDto> models;
        public class ModelDto 
        {
            public T data;
            public Model model;
            public ImmutableList<ChainDto> chains;
        }
        public class ChainDto
        {
            public T data;
            public Chain chain;
            public ImmutableList<ResidueContainerDto<T>> residues;
        }

        public static implicit operator EnsembleDto<T>(EnsembleContainerLayer<T> layer) => new EnsembleDto<T>
        {
            data = layer.me.data,
            pdb = layer.me.pdb,
            models = layer.childs.Select(conf => new ModelDto
            {
                data = conf.me.data,
                model = conf.me.model,
                chains = conf.childs.Select(ch => new ChainDto {
                    data = ch.me.data,
                    chain = ch.me.chain,
                    residues = ch.childs.ToImmutableList()
                }).ToImmutableList()
            }).ToImmutableList()
        };

    }

    //public class EnsembleContainerDto<TResidue, TResidueData, TResidueCont, TChain, TChainData, TChainCont, TModel, TModelData, TModelCont, TProtein, TProteinData>
    //    : EnsembleContainerDto<TProteinData, TProtein>
    //    where TResidue : ResidueDto
    //    where TResidueCont : ResidueContainerDto<TResidueData, TResidue>
    //    where TChain : ChainDto
    //    where TChainCont : ChainContainerDto<TResidue, TResidueData, TResidueCont, TChain, TChainData>
    //    where TModel : ModelDto
    //    where TModelCont : ConformerContainerDto<TResidue, TResidueData, TResidueCont, TChain, TChainData, TChainCont, TModel, TModelData>
    //    where TProtein : ProteinDto
    //{
    //    public List<TModelCont> conformers { get; set; } = new List<TModelCont>();
    //}

    //public class EnsembleContainerDto<T, TProtein, TConformerCont, TConformer, TChainCont, TChain, TResidueCont, TResidue>
    //    : EnsembleContainerDto<TResidue, T, TResidueCont, TChain, T, TChainCont, TConformer, T, TConformerCont, TProtein, T>
    //    where TProtein : ProteinDto
    //    where TConformer : ModelDto
    //    where TChain : ChainDto
    //    where TResidue : ResidueDto
    //    where TResidueCont : ResidueContainerDto<T, TResidue>
    //    where TChainCont : ChainContainerDto<T, TChain, TResidue, TResidueCont>
    //    where TConformerCont : ConformerContainerDto<T, TConformer, TChainCont, TChain, TResidueCont, TResidue>
    //{
        
    //}

    //public class EnsembleContainerDto<T> : EnsembleContainerDto<T, ProteinDto, ConformerContainerDto<T>, ModelDto, ChainContainerDto<T>, ChainDto, ResidueContainerDto<T>, ResidueDto>
    //{

    //}

    //public class EnsembleContainer<T> : EnsembleContainerDto<T, Protein, ConformerContainer<T>, Model, ChainContainer<T>, Chain, ResidueContainer<T>, Residue>
    //{
    //    public void foo<T1, T2, T3, T4, T5>()
    //    {
    //        List < (T1, Protein, List < (T2, Model, List<(T3, Chain, List<(T4, Residue, List<(T5, Atom)>)>)>)>) > q; //alternative model
    //    }
    //}

    

}