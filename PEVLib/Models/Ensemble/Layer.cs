﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Collections.Immutable;

using PEV.Models.Pdb;
using PEV.Calculations;

namespace PEV.Models
{
    public interface ILayer<T, TItem> : IReadonlyLayer<T, TItem>
    {
        new T me { get; set; }
        new IEnumerable<TItem> childs { get; set; }
    }

    public interface IReadonlyLayer<T, TItem>
    {
        T me { get; }
        IEnumerable<TItem> childs { get; }
    }

    public interface ILayer2Readonly<T> : IReadonlyLayer<T, ILayer2Readonly<T>>
    {
        //T me { get; }
        //IEnumerable<ILayer2Readonly<T>> childs { get; }
    }

    public interface ILayer2<T> : ILayer<T, ILayer2<T>>// ILayer2Readonly<T>
    {
        //new T me { get; set; }
        //new IEnumerable<ILayer2<T>> childs { get; set; }
    }

    /// <summary>
    /// just a tag for type safety
    /// </summary>
    public interface IPdbTag
    {

    }

    //basic implementation classes

    public class ImmutableLayer<T, TItem> : IReadonlyLayer<T, TItem>
    {
        public ImmutableLayer(T me, IEnumerable<TItem> childs)
        {
            this.me = me;
            this.childs = childs.ToImmutableList();
        }

        public T me { get; }
        public IImmutableList<TItem> childs { get; }

        IEnumerable<TItem> IReadonlyLayer<T, TItem>.childs => childs;
    }

    public class Layer<T, TItem> : ILayer<T, TItem>
    {
        public T me { get; set; }
        public IList<TItem> childs { get; set; }
        IEnumerable<TItem> ILayer<T, TItem>.childs { get => childs; set => childs = value.ToList(); }

        IEnumerable<TItem> IReadonlyLayer<T, TItem>.childs => childs;

        public static void foo()
        {
            Layer<int, int> f = new Layer<int, int>();
            f.me = 6;
            var q = ((IReadonlyLayer<int, int>)f).me;
            Console.WriteLine(q);
        }
    }

    public class Layer2Readonly<T> : ILayer2Readonly<T>, IReadonlyLayer<T, ILayer2Readonly<T>>
    {
        public T me { get; }

        public IEnumerable<ILayer2Readonly<T>> childs { get; }
    }

    public class Layer2Immutable<T> : ILayer2Readonly<T>
    {
        public T me { get; }

        public IImmutableList<ILayer2Readonly<T>> childs { get; }

        IEnumerable<ILayer2Readonly<T>> IReadonlyLayer<T, ILayer2Readonly<T>>.childs => childs;
    }

    public class Layer2<T> : ILayer2<T>
    {
        public T me { get; set; }
        public IList<ILayer2<T>> childs { get; set; }
        IEnumerable<ILayer2<T>> ILayer<T, ILayer2<T>>.childs { get => childs; set => childs = value.ToList(); }
        IEnumerable<ILayer2<T>> IReadonlyLayer<T, ILayer2<T>>.childs => childs;
    }

    ///// <summary>
    ///// child operations are replaced with noop to ensure that this layer remain the last. (this reduces flexibility but gives more complier checking)
    ///// </summary>
    ///// <typeparam name="T"></typeparam>
    //public class Layer2Last<T> : ILayer2<T>
    //{
    //    public T me { get; set; }
    //    public IEnumerable<ILayer2<T>> childs { get => Enumerable.Empty<ILayer2<T>>(); set { } }
    //}

    public interface IPdbAndData<out TData, out TPdbDto>
       where TPdbDto : IPdbTag
    {
        TData data { get; }
        TPdbDto pdb { get; }
    }

    public class PdbAndData<TData, TPdb> : IPdbAndData<TData, TPdb>
        where TPdb : IPdbTag
    {
        public PdbAndData(TData data, TPdb pdb)
        {
            this.data = data;
            this.pdb = pdb;
        }

        public TData data { get; }

        public TPdb pdb { get; }
    }

    public struct PdbDataContainer<TData, TPdbDto>
        where TPdbDto : IPdbTag
    {
        public TData data;
        public TPdbDto pdbPart;
    }

    public class EnsembleLayer2<T>
        : Layer2<PdbAndData<T, IPdbTag>>
    {

    }

    public class ProteinLayer2
        : Layer2<IPdbTag>
    {

    }

    public static class Layer2Extensions
    {
        public static ILayer2<T2> transform<T1, T2>(this ILayer2<T1> source, Func<T1, T2> transformer) =>
            new Layer2<T2>
            {
                me = transformer(source.me),
                childs = source.childs.Select(x => transform(x, transformer)).ToList()
            };


        ////how to brain fuck
        //public static ILayer2<T2> transform_sliding<T1, T2>(this ILayer2<T1> source, int window_width, Func<IEnumerable<T1>, int, T2> transformer) =>
        //    new Layer2<T2>
        //    {
        //        me = transformer(new T1[] { source.me }, window_width - 1),
        //        childs = source.childs.Window(window_width).Select(tup => new Layer2<T2> {
        //            me = transformer(tup.list.Select(x=>x.me), tup.pos),
        //            childs = 
        //        }
        //    };

    }

    ////public class EnsembleLayer<T>
    ////    : Layer<PdbDataContainer<T, ProteinDto>, Layer<PdbDataContainer<T, ModelDto>, Layer<PdbDataContainer<T, ChainDto>, PdbDataContainer<T, ResidueDto>>>>
    ////{

    ////}

    //public class EnsembleLayer2<T> : Layer2<PdbDataContainer<T, IPdbTag>>
    //{

    //}

    ////takes too much :(
    //public class AtomContainerLayer2Last<T> : Layer2Last<PdbAndData<T, AtomDto>>, ILayer2<IPdbAndData<T, IPdbTag>>
    //{
    //    IPdbAndData<T, IPdbTag> ILayer<IPdbAndData<T, IPdbTag>, ILayer2<IPdbAndData<T, IPdbTag>>>.me { set => me = new PdbAndData<T, AtomDto>(value.data, value.pdb as AtomDto); }

    //    IPdbAndData<T, IPdbTag> IReadonlyLayer<IPdbAndData<T, IPdbTag>, ILayer2<IPdbAndData<T, IPdbTag>>>.me => me;

    //    IEnumerable<ILayer2<IPdbAndData<T, IPdbTag>>> ILayer<IPdbAndData<T, IPdbTag>, ILayer2<IPdbAndData<T, IPdbTag>>>.childs { set { } }

    //    IEnumerable<ILayer2<IPdbAndData<T, IPdbTag>>> IReadonlyLayer<IPdbAndData<T, IPdbTag>, ILayer2<IPdbAndData<T, IPdbTag>>>.childs => null;
    //}

    //public class ResidueContainerLayer2<T> : Layer2<PdbDataContainer<T, IPdbTag>>
    //{

    //}


}

