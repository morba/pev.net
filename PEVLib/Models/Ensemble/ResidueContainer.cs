﻿using System.Runtime.Serialization;
using PEV.Models.Pdb;
using System;
using System.Collections.Generic;
using System.Linq;

namespace PEV.Models.Ensemble
{
    public class ResidueContainerDto<T>
    {
        public T data;
        public Residue residue;
    }

    public class ResidueContainerLayer<T> : Layer<ResidueContainerDto<T>, AtomContainerDto<T>>
    {

    }


    //public class ResidueContainerDto<TAtom, TAtomData, TAtomContainer, TResidue, TResidueData> 
    //    : ResidueContainerDto<TResidueData, TResidue>, ILayer<ResidueContainerDto<TResidueData, TResidue>, TAtomContainer>
    //    where TAtom : AtomDto
    //    where TAtomContainer : AtomContainerDto<TAtomData, TAtom>
    //    where TResidue : ResidueDto
    //{
    //    public List<TAtomContainer> atoms;

    //    public ResidueContainerDto<TResidueData, TResidue> me { get => this; set { data = value.data; residue = value.residue; } }
    //    public IEnumerable<TAtomContainer> childs { get => atoms; set => atoms = value.ToList(); }
    //}

    //public class ResidueContainerDto<TData, TAtom, TAtomContainer, TResidue> 
    //    : ResidueContainerDto<TAtom, TData, TAtomContainer, TResidue, TData>
    //    where TAtom : AtomDto
    //    where TAtomContainer : AtomContainerDto<TData, TAtom>
    //    where TResidue : ResidueDto
    //{

    //}

    ///// <summary>
    ///// for the currenct calculation model. //TODO refactor with layer
    ///// </summary>
    ///// <typeparam name="T"></typeparam>
    //public class ResidueContainerDto<T> : ResidueContainerDto<T, ResidueDto>
    //{

    //}

    ///// <summary>
    ///// for the currenct calculation model. //TODO refactor with layer
    ///// </summary>
    ///// <typeparam name="T"></typeparam>
    //public class ResidueContainer<T> : ResidueContainerDto<T, Residue>
    //{

    //}

}