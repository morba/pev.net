﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Globalization;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net.Http;
using System.Numerics;
using System.Threading.Tasks;
using PEV.Models.Pdb;

namespace PEV.Models.Pdb
{

    public enum AminoAcid : byte
    {
        Null = 0,
        Arg = (byte)'R', //82
        His = (byte)'H',
        Lys = (byte)'K',
        Asp = (byte)'D',
        Glu = (byte)'E',
        Ser = (byte)'S',
        Thr = (byte)'T',
        Asn = (byte)'N',
        Gln = (byte)'Q',
        Cys = (byte)'C',
        Sec = (byte)'U',
        Gly = (byte)'G',
        Pro = (byte)'P',
        Ala = (byte)'A',
        Val = (byte)'V',
        Ile = (byte)'I',
        Leu = (byte)'L',
        Met = (byte)'M',
        Phe = (byte)'F',
        Tyr = (byte)'Y',
        Trp = (byte)'W'
    }

    /// <summary>
    /// static functions for pdb classes (protein mostly) = functions that work on pdb classes, but are not member functions bcs they take or return a "whole" class. 
    /// in contrast member functions only work on a subset of the classes "members"
    /// </summary>
    public static class PdbFun
    {
        /// <summary>
        /// list of 'known' aminoacid names/types ordered alphabeticly
        /// </summary>
        public readonly static string[] aa_list = Enum.GetNames(typeof(AminoAcid)).OrderBy(s => s).Where(s => s != "Null").Select(s => s.ToUpper()).ToArray();

        /// <summary>
        /// directory of amino acid names and their respecting index in <see cref="aa_list"/>. 
        /// for reverse look up scenarios
        /// </summary>
        public readonly static Dictionary<string, int> aa2index = aa_list.ToDictionary(s => s.ToUpper(), s => aa_list.ToList().IndexOf(s));



        [Flags]
        public enum LoadingOptions
        {
            from_cache = 0,
            force_refresh = 1 << 0,
            save_pdb = 1 << 1,
            save_gzip = 1 << 2,
        }

        public static string local_folder = Environment.MachineName == "DESKTOP-2S7C0PJ"
                ? @"F:\repos\PEV.NET\WebAPI\wwwroot\PDB\"
                : Environment.MachineName == "DEV-I5"
                    ? @"D:\repos\PEV.NET\WebAPI\wwwroot\PDB\"
                    : null;

        public static async Task<StreamReader> getStreamFromPdbId(string pdb_id, string local_folder = null, LoadingOptions options = LoadingOptions.save_gzip)
        {
            string local_pdb_path = Path.Combine(local_folder ?? "", pdb_id + ".pdb");
            string local_gzip_path = local_pdb_path + ".gzip";
            if (options.HasFlag(LoadingOptions.force_refresh))
            {
                File.Delete(local_pdb_path);
                File.Delete(local_gzip_path);
                return await getStreamFromPdbId(pdb_id, local_folder, options ^ LoadingOptions.force_refresh);
            }
            if (options.HasFlag(LoadingOptions.save_gzip) && !File.Exists(local_gzip_path))
            {
                using (Stream webstream = await (new HttpClient()).GetStreamAsync($"https://files.rcsb.org/download/{pdb_id}.pdb.gz"))
                using (FileStream outfile = File.Create(local_gzip_path))
                {
                    await webstream.CopyToAsync(outfile);
                }
            }
            if (options.HasFlag(LoadingOptions.save_pdb) && !File.Exists(local_pdb_path))
            {
                using (Stream webstream = await (new HttpClient()).GetStreamAsync($"https://files.rcsb.org/download/{pdb_id}.pdb.gz"))
                using (GZipStream gzip = new GZipStream(webstream, CompressionMode.Decompress))
                using (FileStream outfile = File.Create(local_pdb_path))
                {
                    await gzip.CopyToAsync(outfile);
                }
            }
            if (File.Exists(local_pdb_path))
            {
                return File.OpenText(local_pdb_path);
            }
            else if (File.Exists(local_gzip_path))
            {
                FileStream fs = File.OpenRead(local_gzip_path);
                GZipStream gzip = new GZipStream(fs, CompressionMode.Decompress);
                return new StreamReader(gzip);
            }
            else
            {
                Stream webstream = await (new HttpClient()).GetStreamAsync($"https://files.rcsb.org/download/{pdb_id}.pdb.gz");
                GZipStream gzip = new GZipStream(webstream, CompressionMode.Decompress);
                return new StreamReader(gzip);
            }
        }

        public static async Task<ProteinLayer> getPdbFromId(string pdb_id, string local_folder = null, LoadingOptions options = LoadingOptions.save_gzip)
        {
            StreamReader sr = await getStreamFromPdbId(pdb_id, local_folder, options);
            return getPdbFromStream(sr, pdb_id);
        }

        public static ProteinLayer getPdbFromStream(StreamReader pdb, string pdbId = "")
        {
            Model t_model = null;
            Chain t_chain = null;
            Residue t_residue = null;
            ImmutableList<ModelLayer>.Builder t_models_list = ImmutableList.CreateBuilder<ModelLayer>();
            ImmutableList<ChainLayer>.Builder t_chains_list = ImmutableList.CreateBuilder<ChainLayer>();
            List<AtomData> data_list = new List<AtomData>();
            ImmutableList<ResidueLayer>.Builder t_residue_list = ImmutableList.CreateBuilder<ResidueLayer>();
            ImmutableList<Atom>.Builder t_atom_list = ImmutableList.CreateBuilder<Atom>();
            string title = null;

            while (!pdb.EndOfStream)
            {
                string line = pdb.ReadLine();
                if (line.StartsWith("TITLE"))
                {
                    title += line.Substring(10);
                }

                if (line.StartsWith("MODEL "))
                {
                    t_model = new Model(Int32.Parse(line.Substring(6), CultureInfo.InvariantCulture));
                    data_list = new List<AtomData>();
                }
                if (line.StartsWith("ENDMDL"))
                {
                    if (t_models_list.Count == 0)
                    {
                        //save residue
                        if (t_residue != null)
                        {
                            t_residue_list.Add(new ResidueLayer(t_residue, t_atom_list.ToImmutable()));
                        }

                        //save chain
                        if (t_chain != null)
                        {
                            t_chains_list.Add(new ChainLayer(t_chain, t_residue_list.ToImmutableList()));
                        }

                        //finalize chains (hierachy) later...
                    }
                    //save model
                    if (t_model != null)
                    {
                        t_models_list.Add(new ModelLayer(t_model, data_list.ToArray()));
                    }
                }
                if (line.StartsWith("ATOM  "))
                {
                    if (t_models_list.Count == 0)
                    {
                        int seqNum = Int32.Parse(line.Substring(22, 4), CultureInfo.InvariantCulture);

                        //start of chain
                        if (t_chain == null)
                        {
                            t_chain = new Chain(line[21]);
                        }
                        //start of residue
                        if (t_residue == null)
                        {
                            t_residue = new Residue(seqNum, line.Substring(17, 3));
                        }

                        //end of residue
                        if (t_chain.pdbChainId != line[21] || t_residue.sequenceNumber != seqNum)
                        {
                            //save
                            t_residue_list.Add(new ResidueLayer(t_residue, t_atom_list.ToImmutable()));

                            //create new
                            t_residue = new Residue(seqNum, line.Substring(17, 3));
                            t_atom_list = ImmutableList.CreateBuilder<Atom>();
                        }

                        //end of chain detected
                        if (t_chain.pdbChainId != line[21])
                        {
                            //save
                            t_chains_list.Add(new ChainLayer(t_chain, t_residue_list.ToImmutable()));
                            //create new
                            t_chain = new Chain(line[21]);
                            t_residue_list = ImmutableList.CreateBuilder<ResidueLayer>();
                        }

                        //normal save
                        t_atom_list.Add(new Atom(
                            name: line.Substring(12, 4).Trim(),
                            serialNumber: Int32.Parse(line.Substring(6, 5), CultureInfo.InvariantCulture),
                            element: line.Substring(76, 2).Trim(),
                            dataIndex: data_list.Count));
                    }
                    //always save atom data
                    AtomData d = new AtomData()
                    {
                        coordinates = new Vector3(
                                x: float.Parse(line.Substring(30, 8).Trim(), CultureInfo.InvariantCulture),
                                y: float.Parse(line.Substring(38, 8).Trim(), CultureInfo.InvariantCulture),
                                z: float.Parse(line.Substring(46, 8).Trim(), CultureInfo.InvariantCulture)),
                        occupancy = float.Parse(line.Substring(54, 6).Trim(), CultureInfo.InvariantCulture),
                        tempFactor = float.Parse(line.Substring(60, 6).Trim(), CultureInfo.InvariantCulture)
                    };
                    data_list.Add(d);
                }
            }
            ProteinLayer rtn = new ProteinLayer(new Protein(pdbId, title?.Trim()), t_chains_list.ToImmutable(), t_models_list.ToImmutable());
            return rtn;
        }

    }
}
