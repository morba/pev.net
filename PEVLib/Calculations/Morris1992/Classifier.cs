﻿using System.IO;
using System.Numerics;
using System.Linq;
using Newtonsoft.Json;

using PEV.Models.Pdb;
using PEV.Models.Ensemble;

namespace PEV.Calculations.Morris1992
{

    public class Classifier
    {
        public Classifier()
        {

        }

        public Classifier(string path) => load_config(path);

        private MorrisConfig _conf;


        public void load_config(string path) => _conf = JsonConvert.DeserializeObject<MorrisConfig>(File.ReadAllText(path));

        public char classify(double phi, double psi)
        {
            int x = (int)((phi + 180.0) / _conf.phi_resolution_deg) % (360 / _conf.phi_resolution_deg);
            int y = (int)(-1 * (psi - 180.0) / _conf.psi_resolution_deg) % (360 / _conf.psi_resolution_deg);
            return _conf.matrix[y][x];
        }
    }
}