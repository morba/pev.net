﻿namespace PEV.Calculations.Morris1992
{
    public class MorrisConfig
    {
        public int phi_resolution_deg = 10;
        public int psi_resolution_deg = 10;
        public string[] matrix;
    }
}