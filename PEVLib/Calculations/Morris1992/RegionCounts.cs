﻿using System;

namespace PEV.Calculations.Morris1992
{
    [Flags]
    public enum Region
    {
        core     = 0b0001,
        allowed  = 0b0010,
        generous = 0b0100,
        outside  = 0b1000,
    }

    public class RegionCounts
    {
        public RegionCounts()
        {

        }

        public RegionCounts(char c, long num = 1)
        {
            add(c, num);
        }

        public long core { get; set; }
        public long allowed { get; set; }
        public long generous { get; set; }
        public long outside { get; set; }

        public void add(char c, long num = 1)
        {
            switch(c.ToString().ToUpper()[0])
            {
                case 'C':
                    core += num;
                    break;
                case 'A':
                    allowed += num;
                    break;
                case 'G':
                    generous += num;
                    break;
                case 'O':
                    outside += num;
                    break;
                case ' ':
                    outside += num;
                    break;
                default:
                    throw new System.ArgumentOutOfRangeException(nameof(c));
            }
        }

        public static RegionCounts operator +(RegionCounts mee, RegionCounts other) => new RegionCounts()
        {
            core = mee.core + other.core,
            allowed = mee.allowed + other.allowed,
            generous = mee.generous + other.generous,
            outside = mee.outside + other.outside
        };

        public string printSingle()
        {
            return $"{(core > 0 ? "C" : null)}{(allowed > 0 ? "A" : null)}{(generous > 0 ? "G" : null)}{(outside > 0 ? "O" : null)}";
        }

    }
}