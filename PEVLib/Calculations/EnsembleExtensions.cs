﻿using System;
using System.Linq;
using PEV.Models.Ensemble;
using System.Collections.Generic;
using PEV.Calculations;

namespace PEV.Calculations
{
    public static class EnsembleExtensions
    {
        //public static ILayer2<T2> transform_layer2<T1, T2>(ILayer2<T1> inp, Func<T1, T2> transformer) =>
        //    new Layer2<T2>
        //    {
        //        me = transformer(inp.me),
        //        childs = inp.childs.Select(c => transform_layer2(c, transformer)).ToList()
        //    };
        //public static ILayer2<T2> aggregate<T1, T2>(ILayer2<T1> inp, Func<IEnumerable<T1>, T2> aggregator) =>
        //    new Layer2<T2>
        //    {
        //        me = aggregator(inp.childs?.Select(c=>c.me) ?? Enumerable.Empty<T1>()),
        //        childs = inp.childs?.Select(c => aggregate(c, aggregator)).ToList()
        //    };

        public static EnsembleContainerLayer<T2> transform_ensemble_data<T1, T2>(this EnsembleContainerLayer<T1> inp, Func<T1, T2> transformer) =>
            new EnsembleContainerLayer<T2>() {
                me = new EnsembleContainerDto<T2>()
                {
                    pdb = inp.me.pdb,
                    data = transformer(inp.me.data),
                },
                childs = inp.childs.Select(mod => new ConformerContainerLayer<T2>() {
                    me = new ConformerContainerDto<T2>()
                    {
                        model = mod.me.model,
                        data = transformer(inp.me.data),
                    },
                    childs = mod.childs.Select(ch => new ChainContainerLayer<T2>()
                    {
                        me = new ChainContainerDto<T2>()
                        {
                            chain = ch.me.chain,
                            data = transformer(ch.me.data)
                        },
                        childs = ch.childs.Select(res => new ResidueContainerDto<T2>()
                        {
                            data = transformer(res.data),
                            residue = res.residue
                        }).ToList()
                    }).ToList()
                }).ToList(),
            };

        public static EnsembleContainerLayer<T2> transform_residues_sliding<T1, T2>(this EnsembleContainerLayer<T1> inp, int window_width, Func<IEnumerable<ResidueContainerDto<T1>>, int, ResidueContainerDto<T2>> transformer, Func<int, bool> position_filter = null) =>
            new EnsembleContainerLayer<T2>()
            {
                me = new EnsembleContainerDto<T2>()
                {
                    pdb = inp.me.pdb,
                    data = default(T2)
                },
                childs = inp.childs.Select(mod => new ConformerContainerLayer<T2>()
                {
                    me = new ConformerContainerDto<T2>()
                    {
                        model = mod.me.model,
                        data = default(T2)
                    },
                    childs = mod.childs.Select(ch => new ChainContainerLayer<T2>()
                    {
                        me = new ChainContainerDto<T2>()
                        {
                            chain = ch.me.chain,
                            data = default(T2)
                        },
                        childs = ch.childs.Window(window_width).Where(x => position_filter?.Invoke(x.pos) ?? true).Select(t => transformer(t.list, t.pos)).ToList()
                    }).ToList()
                }).ToList(),
            };

        public static EnsembleContainerLayer<T2> transform_residues<T1, T2>(this EnsembleContainerLayer<T1> inp, Func<ResidueContainerDto<T1>, ResidueContainerDto<T2>> transformer) =>
            new EnsembleContainerLayer<T2>()
            {
                me = new EnsembleContainerDto<T2>()
                {
                    pdb = inp.me.pdb,
                    data = default(T2)
                },
                childs = inp.childs.Select(mod => new ConformerContainerLayer<T2>()
                {
                    me = new ConformerContainerDto<T2>()
                    {
                        model = mod.me.model,
                        data = default(T2)
                    },
                    childs = mod.childs.Select(ch => new ChainContainerLayer<T2>()
                    {
                        me = new ChainContainerDto<T2>()
                        {
                            chain = ch.me.chain,
                            data = default(T2)
                        },
                        childs = ch.childs.Select(res => transformer(res)).ToList()
                    }).ToList()
                }).ToList(),
            };

        public static EnsembleContainerLayer<T2> aggregate<T1, T2>(this EnsembleContainerLayer<T1> source, Func<IEnumerable<T1>, T2> aggregator)
            => new EnsembleContainerLayer<T2>
            {
                me = new EnsembleContainerDto<T2>
                {
                    pdb = source.me.pdb,
                    data = aggregator(source.childs.Select(c=>c.me.data)),
                },
                childs = source.childs.Select(conf => new ConformerContainerLayer<T2>
                {
                    me = new ConformerContainerDto<T2>
                    {
                        model = conf.me.model,
                        data = aggregator(conf.childs.Select(c=>c.me.data)),
                    },
                    childs = conf.childs.Select(ch => new ChainContainerLayer<T2>
                    {
                        me = new ChainContainerDto<T2>
                        {
                            chain = ch.me.chain,
                            data = aggregator(ch.childs.Select(c=>c.data))
                        },
                        childs = ch.childs.Select(res => new ResidueContainerDto<T2> {
                            residue = res.residue,
                            data = aggregator(new T1[] { res.data }),
                        }).ToList()
                    }).ToList()  
                }).ToList()
            };

        public static EnsembleContainerLayer<T3> zip<T1, T2, T3>(this EnsembleContainerLayer<T1> left, EnsembleContainerLayer<T2> right, Func<T1, T2, T3> zipper) =>
            new EnsembleContainerLayer<T3>
            {
                me = new EnsembleContainerDto<T3>
                {
                    pdb = left.me.pdb,
                    data = zipper(left.me.data, right.me.data)
                },
                childs = left.childs.Join(right.childs, x=>x.me.model.pdbModelId, x => x.me.model.pdbModelId, (left_conf, right_conf) => 
                new ConformerContainerLayer<T3> {
                    me = new ConformerContainerDto<T3>
                    {
                        model = left_conf.me.model,
                        data = zipper(left_conf.me.data, right_conf.me.data)
                    },
                    childs = left_conf.childs.Join(right_conf.childs, x=>x.me.chain.pdbChainId, x => x.me.chain.pdbChainId, (left_chain, right_chain) => 
                    new ChainContainerLayer<T3> {
                        me = new ChainContainerDto<T3>
                        {
                            chain = left_chain.me.chain,
                            data = zipper(left_chain.me.data, right_chain.me.data)
                        },
                        childs = left_chain.childs.Join(right_chain.childs, x=>x.residue.sequenceNumber, x => x.residue.sequenceNumber, (left_residue, right_residue) => 
                        new ResidueContainerDto<T3> {
                            residue = left_residue.residue,
                            data = zipper(left_residue.data, right_residue.data)
                        }).ToList()
                    }).ToList()
                }).ToList()
            };


    }


}

