﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace PEV.Calculations
{
    /// <summary>
    /// extends <see cref="IEnumerable{T}"/> with some statistics
    /// </summary>
    public static class StatisticsExtensions
    {
        public static double StdDev(this IEnumerable<double> values)
        {
            //info: https://mathoverflow.net/questions/70345/numerically-most-robust-way-to-compute-sum-of-products-standard-deviation-in-f
            double mean = 0.0;
            double sum = 0.0;
            double stdDev = 0.0;
            int n = 0;
            foreach (double val in values)
            {
                n++;
                double delta = val - mean;
                mean += delta / n;
                sum += delta * (val - mean);
            }
            if (1 < n)
            {
                stdDev = Math.Sqrt(sum / (n - 1));
            }

            return stdDev;
        }

        public static double Median(this IOrderedEnumerable<double> values)
        {
            int count = values.Count();
            return count % 2 == 1 ? values.ElementAt(count / 2) : (values.ElementAt(count / 2 - 1) + values.ElementAt(count / 2)) / 2;
        }

        /// <summary>
        /// returns the (Length * position)th value at the ordered list lineraly extrapolated for real number indexes
        /// </summary>
        /// <param name="values"></param>
        /// <param name="position">eg. for the first quartile use 0.25</param>
        /// <returns></returns>
        public static double Percentile(this IOrderedEnumerable<double> values, double position)
        {
            int count = values.Count();
            double q = (count - 1) * position; //count -1 bcs of indexing. this will guarantee that position = 1 will retrun the last element
            if (q - ((int)q) < double.Epsilon * 10)
            {
                return values.ElementAt((int)q);
            }
            else
            {
                return values.ElementAt((int)q) * (1 - (q - (int)q)) + values.ElementAt((int)q + 1) * (q - (int)q); //calc weighted "average"
            }
        }

        public enum BoundaryCondition
        {
            /// <summary>
            /// return a smaller list where elemnts with missing neighboors are removed
            /// </summary>
            Chomp,
            /// <summary>
            /// uses the default value for the missing neighboors
            /// </summary>
            DefaultValue,
            /// <summary>
            /// uses the first real value for the missing neighboors
            /// </summary>
            ZeroFlux,
            /// <summary>
            /// uses the other end of the list to substitute for missing neighboors
            /// </summary>
            Periodic,
        }
        /// <summary>
        /// windowed select
        /// probably unneccesery helper, 
        /// marks the center of the window as the first and last position that needs to have a real value
        /// </summary>
        /// <typeparam name="T1"></typeparam>
        /// <typeparam name="T2"></typeparam>
        /// <param name="source"></param>
        /// <param name="fun"></param>
        /// <param name="width"></param>
        /// <param name="boundary"></param>
        /// <returns></returns>
        public static IEnumerable<T2> SlidingSelect<T1, T2>(this IEnumerable<T1> source, Func<IEnumerable<T1>, T2> fun, int width = 2, BoundaryCondition boundary = BoundaryCondition.Chomp)
            => source.SlidingSelect(fun, width, width / 2, boundary);

        
        public static IEnumerable<T2> SlidingSelect<T1, T2>(this IEnumerable<T1> source, Func<IEnumerable<T1>, T2> fun, int width, int center, BoundaryCondition boundary = BoundaryCondition.Chomp)
        {
            LinkedList<T1> buffer = new LinkedList<T1>();
            switch (boundary)
            {
                case BoundaryCondition.Chomp:
                    foreach (T1 it in source)
                    {
                        if (buffer.Count < width)
                        {
                            buffer.AddLast(it);
                        }
                        else
                        {
                            yield return fun(buffer);
                            buffer.AddLast(it);
                            buffer.RemoveFirst();
                        }
                    }
                    yield return fun(buffer); //the last element
                    yield break;
                case BoundaryCondition.DefaultValue:
                    //throw new NotImplementedException(); //i cant get this work now. need to play out all the edge cases //maybe write tests first?
                    foreach(T2 it in source.Window(width).Where(x=>x.pos <= center && x.pos >= -center).Select((t) =>
                    {
                        IEnumerable<T1> list = t.list;
                        int pos = t.pos;
                        if (pos > 0)
                        {
                            return fun(Enumerable.Range(0, pos).Select(x => default(T1)).Concat(list));
                        }
                        else if (pos < 0)
                        {
                            return fun(list.Concat(Enumerable.Range(pos, 0).Select(x => default(T1))));
                        }
                        else
                        {
                            return fun(list);
                        }
                    }))
                    {
                        yield return it;
                    }
                    yield break;
                case BoundaryCondition.ZeroFlux:
                    T1 first = source.FirstOrDefault();
                    T1 last = source.LastOrDefault();
                    foreach (T2 it in source.Window(width).Where(x => x.pos <= center && x.pos >= -center).Select((t) =>
                    {
                        IEnumerable<T1> list = t.list;
                        int pos = t.pos;
                        if (pos > 0)
                        {
                            return fun(Enumerable.Range(0, pos).Select(x => first).Concat(list));
                        }
                        else if (pos < 0)
                        {
                            return fun(list.Concat(Enumerable.Range(pos, 0).Select(x => last)));
                        }
                        else
                        {
                            return fun(list);
                        }
                    }))
                    {
                        yield return it;
                    }
                    yield break;
                case BoundaryCondition.Periodic:
                    IEnumerable<T1> begining = source.Take(width).ToArray(); //double enumeration is bad but i have no other choice :(
                    IEnumerable<T1> ending = source.Reverse().Take(width).ToArray();
                    foreach (T2 it in source.Window(width).Where(x => x.pos <= center && x.pos >= -center).Select((t) =>
                    {
                        IEnumerable<T1> list = t.list;
                        int pos = t.pos;
                        if (pos > 0)
                        {
                            return fun(ending.Take(pos).Concat(list));
                        }
                        else if (pos < 0)
                        {
                            return fun(list.Concat(begining.Take(-pos)));
                        }
                        else
                        {
                            return fun(list);
                        }
                    }))
                    {
                        yield return it;
                    }
                    yield break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(boundary));
            }
        }

        /// <summary>
        /// solve your own boundary condition version
        /// </summary>
        /// <typeparam name="T1"></typeparam>
        /// <typeparam name="T2"></typeparam>
        /// <param name="source"></param>
        /// <param name="fun"></param>
        /// <param name="width"></param>
        /// <param name="shift"></param>
        /// <returns></returns>
        public static IEnumerable<T2> SlidingSelect<T1, T2>(this IEnumerable<T1> source, Func<IEnumerable<T1>, int, T2> fun, int width)
        {
            LinkedList<T1> buffer = new LinkedList<T1>();
            foreach(T1 it in source)
            {
                buffer.AddLast(it);
                if(buffer.Count > width)
                {
                    buffer.RemoveFirst();
                }
                yield return fun(buffer, width - buffer.Count);
            }
            while(buffer.Count > 1)
            {
                buffer.RemoveFirst();
                yield return fun(buffer, buffer.Count - width);
            }
            yield break;
        }

        /// <summary>
        /// window with number of (signed) missing elements/neighboors 
        /// (+ missing from the end of the buffer, - missing from the begining of the buffer)
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source"></param>
        /// <param name="width"></param>
        /// <returns></returns>
        public static IEnumerable<(IEnumerable<T> list, int pos)> Window<T>(this IEnumerable<T> source, int width)
        {
            LinkedList<T> buffer = new LinkedList<T>();
            foreach (T it in source)
            {
                buffer.AddLast(it);
                if (buffer.Count > width)
                {
                    buffer.RemoveFirst();
                }
                yield return (buffer, width - buffer.Count);
            }
            while (buffer.Count > 1)
            {
                buffer.RemoveFirst();
                yield return (list: buffer, pos: buffer.Count - width);
            }
            yield break;
        }

        public static IEnumerable<T2> ZipMany<T1, T2>(this IEnumerable<IEnumerable<T1>> source, Func<IEnumerable<T1>, T2> zipper)
        {
            IEnumerator<T1>[] enumerators = source.Select(x => x.GetEnumerator()).ToArray();
            while ( enumerators.Aggregate(true, (acc, item) => acc && item.MoveNext() ) )
            {
                yield return zipper(enumerators.Select(x => x.Current));
            }
            yield break;
        }


    }
}
