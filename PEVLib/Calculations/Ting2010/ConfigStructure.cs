﻿namespace PEV.Calculations.Ting2010
{
    public struct ConfigStructure
    {
        public float probablity;
        public float log_probability;
        public float cumulative_sum;
    }
}