﻿using System.Collections.Generic;
using PEV.Models;
using System.IO;
using System;
using System.Runtime.Serialization;
using PEV.Models.Ensemble;
using PEV.Models.Pdb;
using System.Numerics;

namespace PEV.Calculations.Ting2010
{
    /// <summary>
    /// Neighboor Dependent Ramachandran Distribution
    /// </summary>
    public class NDRD : RamachandranDistribution
    {
        /// <summary>
        /// true for right neighboor, false for left. default is left.
        /// </summary>
        public bool right_neighboor = false;

        /// <summary>
        /// map of aminoacid residue to phi x psi matrix of probability
        /// </summary>
        public Dictionary<string, Dictionary<string, float[][]>> probability { get; set; } = new Dictionary<string, Dictionary<string, float[][]>>();

        /// <summary>
        /// cache in the ln(probability) = convert CPU time to Memory
        /// can be null.
        /// </summary>
        [IgnoreDataMember]
        public Dictionary<string, Dictionary<string, float[][]>> log_probability { get; set; }

        public void load_cache()
        {
            log_probability = new Dictionary<string, Dictionary<string, float[][]>>();
            foreach (KeyValuePair<string, Dictionary<string, float[][]>> kvp in probability)
            {
                log_probability[kvp.Key] = new Dictionary<string, float[][]>();
                foreach (KeyValuePair<string, float[][]> kvp2 in kvp.Value)
                {
                    log_probability[kvp.Key][kvp2.Key] = new float[kvp2.Value.Length][];
                    for (int i = 0; i < kvp2.Value.Length; i++)
                    {
                        log_probability[kvp.Key][kvp2.Key][i] = new float[kvp2.Value[i].Length];
                        for (int j = 0; j < kvp2.Value[i].Length; j++)
                        {
                            log_probability[kvp.Key][kvp2.Key][i][j] = (float)Math.Log(kvp2.Value[i][j]);
                        }
                    }
                }
            }
        }

        public static NDRD fromJson(string path, bool load_cache = true)
        {
            string jsonValue = File.ReadAllText(path);
            NDRD rtn = Newtonsoft.Json.JsonConvert.DeserializeObject<NDRD>(jsonValue);
            if(load_cache)
            {
                rtn.load_cache();
            }
            return rtn;
        }

        public double get_probability_for_residue(ResidueContainerDto<Vector2> center, ResidueContainerDto<Vector2> neighboor)
        {
            int x = (int)((center.data.X + (180 - phi_resolution / 2.0)) / phi_resolution) % (360 / phi_resolution);
            int y = (int)((center.data.Y + (180 - psi_resolution / 2.0)) / psi_resolution) % (360 / psi_resolution);
            return probability[center.residue.name][neighboor.residue.name][x][y];
        }

        public double get_log_probability_for_residue(ResidueContainerDto<Vector2> center, ResidueContainerDto<Vector2> neighboor)
        {
            int x = (int)((center.data.X + (180 - phi_resolution / 2.0)) / phi_resolution) % (360 / phi_resolution);
            int y = (int)((center.data.Y + (180 - psi_resolution / 2.0)) / psi_resolution) % (360 / psi_resolution);
            return log_probability?[center.residue.name]?[neighboor.residue.name]?[x]?[y] ?? Math.Log(probability[center.residue.name][neighboor.residue.name][x][y]);
        }

        public void load_wasting_datafile(string path, string direction = "left")
        {
            right_neighboor = direction == "right";
            foreach (string aa in PdbFun.aa_list)
            {
                probability[aa] = new Dictionary<string, float[][]>();
                foreach (string aa2 in PdbFun.aa_list)
                {
                    probability[aa][aa2] = new float[(int)(360 / phi_resolution)][];
                    for (int i = 0; i < 360 / phi_resolution; i++)
                    {
                        probability[aa][aa2][i] = new float[(int)(360 / psi_resolution)];
                    }
                }
            }
            log_probability = new Dictionary<string, Dictionary<string, float[][]>>();
            foreach (string aa in PdbFun.aa_list)
            {
                log_probability[aa] = new Dictionary<string, float[][]>();
                foreach (string aa2 in PdbFun.aa_list)
                {
                    log_probability[aa][aa2] = new float[(int)(360 / phi_resolution)][];
                    for (int i = 0; i < 360 / phi_resolution; i++)
                    {
                        log_probability[aa][aa2][i] = new float[(int)(360 / psi_resolution)];
                    }
                }
            }
            using (StreamReader sr = File.OpenText(path))
            {
                while (!sr.EndOfStream)
                {
                    string line = sr.ReadLine();
                    if (string.IsNullOrWhiteSpace(line) || line.StartsWith("#"))
                    {
                        continue;
                    }

                    string[] columns = line.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                    if (columns.Length < 8
                        || !columns[1].Equals(direction)
                        || !PdbFun.aa2index.ContainsKey(columns[0])
                        || !PdbFun.aa2index.ContainsKey(columns[2]))
                    {
                        continue;
                    }
                    probability[columns[0]][columns[2]]
                        [(int)((int.Parse(columns[3]) + 180) / phi_resolution)]
                        [(int)((int.Parse(columns[4]) + 180) / psi_resolution)]
                        = float.Parse(columns[5], System.Globalization.NumberFormatInfo.InvariantInfo);
                    log_probability[columns[0]][columns[2]]
                        [(int)((int.Parse(columns[3]) + 180) / phi_resolution)]
                        [(int)((int.Parse(columns[4]) + 180) / psi_resolution)]
                        = float.Parse(columns[6], System.Globalization.NumberFormatInfo.InvariantInfo);
                }
            }
        }

    }
}