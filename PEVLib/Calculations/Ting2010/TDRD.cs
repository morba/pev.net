﻿using PEV.Models;
using System.IO;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using PEV.Models.Ensemble;
using PEV.Models.Pdb;
using System.Numerics;

namespace PEV.Calculations.Ting2010
{
    /// <summary>
    /// Type Dependent Ramachandran Distribution
    /// </summary>
    public class TDRD : RamachandranDistribution
    {
        /// <summary>
        /// map of aminoacid residue to phi x psi matrix of probability
        /// </summary>
        public Dictionary<string, float[][]> probability { get; set; } = new Dictionary<string, float[][]>();

        /// <summary>
        /// cache in the ln(probability) = convert CPU time to Memory
        /// can be null.
        /// </summary>
        [IgnoreDataMember]
        private Dictionary<string, float[][]> log_probability { get; set; }

        public static TDRD fromJson(string path, bool load_cache = true)
        {
            string jsonValue = File.ReadAllText(path);
            TDRD rtn = Newtonsoft.Json.JsonConvert.DeserializeObject<TDRD>(jsonValue);
            if (load_cache)
            {
                rtn.load_cache();
            }
            return rtn;
        }

        public void load_cache()
        {
            log_probability = new Dictionary<string, float[][]>();
            foreach (KeyValuePair<string, float[][]> kvp in probability)
            {
                log_probability[kvp.Key] = new float[kvp.Value.Length][];
                for (int i = 0; i < kvp.Value.Length; i++)
                {
                    log_probability[kvp.Key][i] = new float[kvp.Value[i].Length];
                    for (int j = 0; j < kvp.Value[i].Length; j++)
                    {
                        log_probability[kvp.Key][i][j] = (float)Math.Log(kvp.Value[i][j]);
                    }
                }
            }
        }

        public double get_probability_for_residue(ResidueContainerDto<Vector2> r)
        {
            int x = (int)((r.data.X + (180 - phi_resolution / 2.0)) / phi_resolution) % (360 / phi_resolution);
            int y = (int)((r.data.Y + (180 - psi_resolution / 2.0)) / psi_resolution) % (360 / psi_resolution);
            return probability[r.residue.name][x][y];
        }

        public double get_log_probability_for_residue(ResidueContainerDto<Vector2> r)
        {
            int x = (int)((r.data.X + (180 - phi_resolution / 2.0)) / phi_resolution) % (360 / phi_resolution);
            int y = (int)((r.data.Y + (180 - psi_resolution / 2.0)) / psi_resolution) % (360 / psi_resolution);
            return log_probability?[r.residue.name]?[x]?[y] ?? Math.Log(probability[r.residue.name][x][y]);
        }

        public void load_wasting_datafile(string path, string direction = "left")
        {
            foreach (string aa in PdbFun.aa_list)
            {
                probability[aa] = new float[(int)(360 / phi_resolution)][];
                for (int i = 0; i < 360 / phi_resolution; i++)
                {
                    probability[aa][i] = new float[(int)(360 / psi_resolution)];
                }
            }
            log_probability = new Dictionary<string, float[][]>();
            foreach (string aa in PdbFun.aa_list)
            {
                log_probability[aa] = new float[(int)(360 / phi_resolution)][];
                for (int i = 0; i < 360 / phi_resolution; i++)
                {
                    log_probability[aa][i] = new float[(int)(360 / psi_resolution)];
                }
            }
            using (StreamReader sr = File.OpenText(path))
            {
                while (!sr.EndOfStream)
                {
                    string line = sr.ReadLine();
                    if (string.IsNullOrWhiteSpace(line) || line.StartsWith("#")) continue;
                    string[] columns = line.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                    if (columns.Length < 8
                        || !columns[1].Equals(direction)
                        || !columns[2].Equals("ALL")
                        || !PdbFun.aa2index.ContainsKey(columns[0]))
                    {
                        continue;
                    }
                    probability[columns[0]]
                        [(int)((int.Parse(columns[3]) + 180) / phi_resolution)]
                        [(int)((int.Parse(columns[4]) + 180) / psi_resolution)]
                        = float.Parse(columns[5], System.Globalization.NumberFormatInfo.InvariantInfo);
                    log_probability[columns[0]]
                        [(int)((int.Parse(columns[3]) + 180) / phi_resolution)]
                        [(int)((int.Parse(columns[4]) + 180) / psi_resolution)]
                        = float.Parse(columns[6], System.Globalization.NumberFormatInfo.InvariantInfo);
                }
            }
        }
    }
}