﻿using PEV.Models.Ensemble;
using System.Linq;
using System.Numerics;

namespace PEV.Calculations.Ting2010
{
    public abstract class RamachandranDistribution
    {
        /// <summary>
        /// phi_resolution in degrees
        /// </summary>
        public int phi_resolution { get; } = 5;
        /// <summary>
        /// psi_resolution in degrees
        /// </summary>
        public int psi_resolution { get; } = 5;

        //public abstract double get_probability_for_residue(ResidueContainer<Vector2> r);

        //public abstract double get_log_probability_for_residue(ResidueContainer<Vector2> r);

    }
}