﻿using System.Linq;
using System.Collections.Generic;

namespace PEV.Calculations.Ting2010
{
    public class LogProbStats
    {
        /// <summary>
        /// average log probability
        /// </summary>
        public double mean;
        /// <summary>
        /// sample standard deviation of log probabilities
        /// </summary>
        public double sample_std_dev;
        /// <summary>
        /// a list of percentiles from 0% to 100% in liner increase 
        /// (eg if the list contains 3 elements the you have the [min, median, max] if it contains 11 elemnts then you have [min, 10%, 20%, ... median, 60%, ... max])
        /// percentiles are linearly extrapolated when their position fall beetwen 2 integers 
        /// (eg. for a list of 12 items the 50%=median is 0.5*6th item + 0.5*7th item)
        /// </summary>
        public List<double> percentiles = new List<double>();

        public LogProbStats()
        {

        }

        public LogProbStats(IOrderedEnumerable<double> data, int percentile_resolution = 10)
        {
            mean = data.Average();
            sample_std_dev = data.StdDev();
            for(double i = 0.0; i <= percentile_resolution; i++)
            {
                percentiles.Add(data.Percentile(i / percentile_resolution));
            }
        }

        public LogProbStats(IEnumerable<double> data, int percentile_resolution = 10, bool skip_infinity = false) 
            : this((skip_infinity? data.Where(x=>!double.IsInfinity(x)) : data).OrderBy(x => x), percentile_resolution)
        {
            
        }
    }
}