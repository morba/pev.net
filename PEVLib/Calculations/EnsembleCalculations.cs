﻿using System;
using System.Text;
using System.Numerics;
using System.Linq;
using System.Collections.Generic;

using PEV.Models.Pdb;
using PEV.Models.Ensemble;
using PEV.Calculations.Morris1992;
using PEV.Calculations.Ting2010;
using System.Reflection;
using PEV.Models;

namespace PEV.Calculations
{
    public static class EnsembleCalculations
    {
        /// <summary>
        /// Praxeolitic formula 
        /// 1 sqrt, 1 cross product
        /// https://stackoverflow.com/questions/20305272/dihedral-torsion-angle-from-four-points-in-cartesian-coordinates-in-python
        /// </summary>
        /// <param name="p0"></param>
        /// <param name="p1"></param>
        /// <param name="p2"></param>
        /// <param name="p3"></param>
        /// <returns></returns>
        public static double dihedral_angle(Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3)
        {
            Vector3 b0 = -1.0f * (p1 - p0);
            Vector3 b1 = p2 - p1;
            Vector3 b2 = p3 - p2;

            //normalize b1 so that it does not influence magnitude of vector
            //rejections that come next
            b1 = Vector3.Normalize(b1);

            // vector rejections
            // v = projection of b0 onto plane perpendicular to b1
            //   = b0 minus component that aligns with b1
            // w = projection of b2 onto plane perpendicular to b1
            //   = b2 minus component that aligns with b1
            Vector3 v = b0 - Vector3.Dot(b0, b1) * b1;
            Vector3 w = b2 - Vector3.Dot(b2, b1) * b1;

            //# angle between v and w in a plane is the torsion angle
            //# v and w may not be normalized but that's fine since tan is y/x
            float x = Vector3.Dot(v, w);
            float y = Vector3.Dot(Vector3.Cross(b1, v), w);
            return Math.Atan2(y, x) * (180 / Math.PI);
        }

        /// <summary>
        /// simple helper to determine if a number is stored with only 1 bit set to 1
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool isFlag(long value)
        {
            //assuming 2s complement number storage
            if (value < 0 && value != long.MinValue) return false;
            bool first = false;
            while (value > 0)
            {
                if (value % 2 > 0)
                {
                    if (first) return false;
                    else first = true;
                }
                value = value >> 1;
            }
            return first;
        }

        public static EnsembleContainerLayer<Vector2> dihedral_angles(ProteinLayer pdb) =>
            new EnsembleContainerLayer<Vector2>()
            {
                me = new EnsembleContainerDto<Vector2>
                {
                    pdb = pdb.me,
                },
                childs = pdb.models.Select(model => new ConformerContainerLayer<Vector2>
                {
                    me = new ConformerContainerDto<Vector2>
                    {
                        model = model.me,
                    },
                    childs = pdb.childs.Where(ch => ch.childs.Count() > 2).Select(ch => new ChainContainerLayer<Vector2>
                    {
                        me = new ChainContainerDto<Vector2>
                        {
                            chain = ch.me
                        },
                        childs = ch.childs.Window(3).Where(t => t.pos < 2 && t.pos > -2).Select(t => new ResidueContainerDto<Vector2>
                        {
                            residue = t.list.ElementAt(t.pos > 0 ? 0 : 1).me,
                            data =
                              new Vector2(
                                  x: t.pos <= 0 ? (float)dihedral_angle( //pos <= 0 --> we are not missing elements from the begining of the window
                                  p0: model.childs[t.list.ElementAt(0).atomMap["C"].dataIndex].coordinates,
                                  p1: model.childs.ElementAt(t.list.ElementAt(1).atomMap["N"].dataIndex).coordinates,
                                  p2: model.childs.ElementAt(t.list.ElementAt(1).atomMap["CA"].dataIndex).coordinates,
                                  p3: model.childs.ElementAt(t.list.ElementAt(1).atomMap["C"].dataIndex).coordinates)
                                  : float.NaN,
                                  y: t.pos >= 0 ? (float)dihedral_angle( //pos >= 0 --> we are not missing elements from the end of the window
                                  p0: model.childs.ElementAt(t.list.ElementAt(1 - t.pos).atomMap["N"].dataIndex).coordinates,
                                  p1: model.childs.ElementAt(t.list.ElementAt(1 - t.pos).atomMap["CA"].dataIndex).coordinates,
                                  p2: model.childs.ElementAt(t.list.ElementAt(1 - t.pos).atomMap["C"].dataIndex).coordinates,
                                  p3: model.childs.ElementAt(t.list.ElementAt(2 - t.pos).atomMap["N"].dataIndex).coordinates)
                                  : float.NaN
                                  )
                        }).ToList()
                    }).ToList()
                }).ToList()
            };

        public static EnsembleContainerLayer<RegionCounts> classify_ensemble(EnsembleContainerLayer<Vector2> source, Classifier classifier) =>
            source
                .transform_ensemble_data(dihedrals => (float.IsNaN(dihedrals.X) || float.IsNaN(dihedrals.Y) ) ? new RegionCounts() : new RegionCounts(classifier.classify(dihedrals.X, dihedrals.Y)))
                .aggregate(x => x.Aggregate(new RegionCounts(), (acc, item) => acc + item));

        public static EnsembleContainerLayer<double> get_log_prob_for_ensemble(EnsembleContainerLayer<Vector2> source, TDRD tdrd) =>
            source.transform_residues(dihedrals => new ResidueContainerDto<double>
            {
                data = float.IsNaN(dihedrals.data.LengthSquared()) ? double.NaN : tdrd.get_log_probability_for_residue(dihedrals),
                residue = dihedrals.residue
            });

        public static EnsembleContainerLayer<double> get_log_prob_for_ensemble(EnsembleContainerLayer<Vector2> source, NDRD ndrd) =>
            ndrd.right_neighboor
            ?
            source.transform_residues_sliding(2, (residues, pos) =>
            float.IsNaN(residues.ElementAt(0).data.X) || float.IsNaN(residues.ElementAt(0).data.Y)
            ? new ResidueContainerDto<double>() { data = double.NaN, residue = residues.ElementAt(0).residue }
            : new ResidueContainerDto<double>
            {
                data = ndrd.get_log_probability_for_residue(
                        center: residues.ElementAt(0),
                        neighboor: residues.ElementAt(1)),
                residue = residues.ElementAt(0).residue
            }, x => x == 0)
            :
            source.transform_residues_sliding(2, (residues, pos) =>
            float.IsNaN(residues.ElementAt(1).data.X) || float.IsNaN(residues.ElementAt(1).data.Y)
            ? new ResidueContainerDto<double>() { data = double.NaN, residue = residues.ElementAt(1).residue }
            : new ResidueContainerDto<double>
            {
                data = ndrd.get_log_probability_for_residue(
                        center: residues.ElementAt(1),
                        neighboor: residues.ElementAt(0)),
                residue = residues.ElementAt(1).residue
            }, x => x == 0);

        public static EnsembleContainerLayer<LogProbStats> get_log_prob_stats(EnsembleContainerLayer<double> source) =>
            source.aggregate(vector => new LogProbStats(vector.OrderBy(x => x)));

        public static void add_ensemble_model<T>(EnsembleContainerLayer<T> source, Func<IEnumerable<T>, T> ensembler) =>
            source.childs.Add(create_ensemble_model(source, ensembler));

        public static ConformerContainerLayer<T> create_ensemble_model<T>(EnsembleContainerLayer<T> source, Func<IEnumerable<T>, T> ensembler) =>
            new ConformerContainerLayer<T>
            {
                me = new ConformerContainerDto<T>
                {
                    model = new Model(int.MinValue),
                    data = ensembler(source.childs.Select(c => c.me.data)),
                },
                childs = source.childs.SelectMany(m => m.childs).GroupBy(c => c.me.chain.pdbChainId).Select(chains => new ChainContainerLayer<T>
                {
                    me = new ChainContainerDto<T>
                    {
                        data = ensembler(chains.Select(ch => ch.me.data)),
                        chain = new Chain(chains.Key)
                    },
                    childs = chains.SelectMany(ch => ch.childs).GroupBy(res => res.residue.sequenceNumber).Select(residues => new ResidueContainerDto<T>
                    {
                        data = ensembler(residues.Select(res => res.data)),
                        residue = residues.First().residue
                    }).ToList()
                }).ToList()
            };

        public static IEnumerable<string> plot_ramachandran(EnsembleContainerLayer<Vector2> source) =>
            source.childs.SelectMany(m => m.childs.SelectMany(c => c.childs.Select(r =>
                $"{r.data.X.ToString("G6", System.Globalization.CultureInfo.InvariantCulture)} {r.data.Y.ToString("G6", System.Globalization.CultureInfo.InvariantCulture)} {r.residue.name}")));

        public static IEnumerable<string> plot_data_by_model<T>(EnsembleContainerLayer<T> source) =>
            new string[] { $"{source.childs.Aggregate("", (acc, item) => $"{acc}#{item.me.model.pdbModelId} ")}#seqNum #AAtype" }.Concat(
            source.childs.Select(x => x.childs.SelectMany(ch => ch.childs)).ZipMany(x => x.Aggregate("", (acc, item) => acc + item.data.ToString() + " ") + $"{x.First().residue.sequenceNumber} {x.First().residue.name}"));

        public static IEnumerable<string> plot_data_with_id<T>(EnsembleContainerLayer<T> source) =>
            new string[] { $"#pdbId: {source.me.pdb.pdbId}" }.Concat(
                    source.childs.SelectMany(conf => (new string[] { $"#modelId: {conf.me.model.pdbModelId}" }).Concat(
                        conf.childs.SelectMany(ch => new string[] { $"#chainId: {ch.me.chain.pdbChainId}" }.Concat(
                            ch.childs.Select(res=> $"{res.data.ToString()} #residueSeqId: {res.residue.sequenceNumber} {res.residue.name}")
                            )
                        )
                    )
                ));

        //public static IEnumerable<string> plot_ramachandran_vs_tdrd(EnsembleContainerLayer<)



        //public static EnsembleContainerLayer<T2> create_ensemble_model<T1, T2>(EnsembleContainerLayer<T1> source, Func<IEnumerable<T1>, T2> ensembler) =>



        //public static void iterate_ensemble_containers<T1, T2>(EnsembleContainer<T1> a, EnsembleContainer<T2> b, Action<ResidueContainer<T1>, ResidueContainer<T2>> fun)
        //{
        //    int a_conf_index = 0;
        //    int b_conf_index = 0;
        //    while (a_conf_index < a.conformers.Count && b_conf_index < b.conformers.Count)
        //    {
        //        ConformerContainer<T1> a_conf = a.conformers[a_conf_index];
        //        ConformerContainer<T2> b_conf = b.conformers[b_conf_index];
        //        if (a_conf.model == b_conf.model)
        //        {
        //            int a_chain_index = 0;
        //            int b_chain_index = 0;
        //            while (a_chain_index < a_conf.chains.Count && b_chain_index < b_conf.chains.Count)
        //            {
        //                ChainContainer<T1> a_chain = a_conf.chains[a_chain_index];
        //                ChainContainer<T2> b_chain = b_conf.chains[b_chain_index];
        //                if (a_chain.chain == b_chain.chain)
        //                {
        //                    int a_residue_index = 0;
        //                    int b_residue_index = 0;
        //                    while (a_residue_index < a_chain.residues.Count && b_residue_index < b_chain.residues.Count)
        //                    {
        //                        ResidueContainer<T1> a_residue = a_chain.residues[a_residue_index];
        //                        ResidueContainer<T2> b_residue = b_chain.residues[b_residue_index];
        //                        if (a_residue.residue == b_residue.residue)
        //                        {
        //                            fun(a_residue, b_residue);
        //                            a_residue_index++;
        //                        }
        //                        else if (a_residue.residue.index < b_residue.residue.index)
        //                        {
        //                            a_residue_index++;
        //                        }
        //                        else if (b_residue.residue.index < a_residue.residue.index)
        //                        {
        //                            b_residue_index++;
        //                        }
        //                        else
        //                        {
        //                            throw new Exception("order assertion failed at residue level");
        //                        }

        //                    }
        //                    a_chain_index++;
        //                }
        //                else if (a_chain.chain.index < b_chain.chain.index)
        //                {
        //                    a_chain_index++;
        //                }
        //                else if (a_chain.chain.index > b_chain.chain.index)
        //                {
        //                    b_chain_index++;
        //                }
        //                else
        //                {
        //                    throw new Exception("order assertion failed at chain level");
        //                }
        //            }
        //            a_conf_index++;
        //        }
        //        else if (a_conf.model.pdbModelId < b_conf.model.pdbModelId)
        //        {
        //            a_conf_index++;
        //        }
        //        else if (a_conf.model.pdbModelId > b_conf.model.pdbModelId)
        //        {
        //            b_conf_index++;
        //        }
        //        else
        //        {
        //            throw new Exception("order assertion failed at conformer level");
        //        }
        //    }
        //}

        //public static EnsembleContainer<(T1, T2)> merge_ensemble_containers<T1, T2>(EnsembleContainer<T1> a, EnsembleContainer<T2> b)
        //{
        //    EnsembleContainer<(T1, T2)> c = new EnsembleContainer<(T1, T2)>()
        //    {
        //        pdb = a.pdb,
        //        data = (a.data, b.data),
        //        conformers = new List<ConformerContainer<(T1, T2)>>(),
        //    };

        //    int a_conf_index = 0;
        //    int b_conf_index = 0;
        //    while (a_conf_index < a.conformers.Count && b_conf_index < b.conformers.Count)
        //    {
        //        ConformerContainer<T1> a_conf = a.conformers[a_conf_index];
        //        ConformerContainer<T2> b_conf = b.conformers[b_conf_index];
        //        ConformerContainer<(T1, T2)> c_conf = new ConformerContainer<(T1, T2)>()
        //        {
        //            model = a_conf.model,
        //            data = (a_conf.data, b_conf.data),
        //            chains = new List<ChainContainer<(T1, T2)>>()
        //        };
        //        c.conformers.Add(c_conf);
        //        if (a_conf.model == b_conf.model)
        //        {
        //            int a_chain_index = 0;
        //            int b_chain_index = 0;
        //            while (a_chain_index < a_conf.chains.Count && b_chain_index < b_conf.chains.Count)
        //            {
        //                ChainContainer<T1> a_chain = a_conf.chains[a_chain_index];
        //                ChainContainer<T2> b_chain = b_conf.chains[b_chain_index];
        //                ChainContainer<(T1, T2)> c_chain = new ChainContainer<(T1, T2)>()
        //                {
        //                    chain = a_chain.chain,
        //                    data = (a_chain.data, b_chain.data),
        //                    residues = new List<ResidueContainer<(T1, T2)>>()
        //                };
        //                c_conf.chains.Add(c_chain);
        //                if (a_chain.chain == b_chain.chain)
        //                {
        //                    int a_residue_index = 0;
        //                    int b_residue_index = 0;
        //                    while (a_residue_index < a_chain.residues.Count && b_residue_index < b_chain.residues.Count)
        //                    {
        //                        ResidueContainer<T1> a_residue = a_chain.residues[a_residue_index];
        //                        ResidueContainer<T2> b_residue = b_chain.residues[b_residue_index];
        //                        if (a_residue.residue == b_residue.residue)
        //                        {
        //                            c_chain.residues.Add(new ResidueContainer<(T1, T2)>()
        //                            {
        //                                residue = a_residue.residue,
        //                                data = (a_residue.data, b_residue.data)
        //                            });
        //                            a_residue_index++;
        //                        }
        //                        else if (a_residue.residue.index < b_residue.residue.index)
        //                        {
        //                            a_residue_index++;
        //                        }
        //                        else if (b_residue.residue.index < a_residue.residue.index)
        //                        {
        //                            b_residue_index++;
        //                        }
        //                        else
        //                        {
        //                            throw new Exception("order assertion failed at residue level");
        //                        }

        //                    }
        //                    a_chain_index++;
        //                }
        //                else if (a_chain.chain.index < b_chain.chain.index)
        //                {
        //                    a_chain_index++;
        //                }
        //                else if (a_chain.chain.index > b_chain.chain.index)
        //                {
        //                    b_chain_index++;
        //                }
        //                else
        //                {
        //                    throw new Exception("order assertion failed at chain level");
        //                }
        //            }
        //            a_conf_index++;
        //        }
        //        else if (a_conf.model.pdbModelId < b_conf.model.pdbModelId)
        //        {
        //            a_conf_index++;
        //        }
        //        else if (a_conf.model.pdbModelId > b_conf.model.pdbModelId)
        //        {
        //            b_conf_index++;
        //        }
        //        else
        //        {
        //            throw new Exception("order assertion failed at conformer level");
        //        }
        //    }
        //    return c;
        //}

        ///// <summary>
        ///// merges 2 ensemble container and prints its data in the same iteration
        ///// </summary>
        ///// <param name="morris"></param>
        ///// <param name="logprob"></param>
        ///// <returns></returns>
        //public static StringBuilder plot_morris_vs_logprob_fast(EnsembleContainer<RegionCounts> morris, EnsembleContainer<double> logprob)
        //{
        //    StringBuilder sb = new StringBuilder();
        //    int morris_conf_index = 0;
        //    int tdrd_conf_index = 0;
        //    while (morris_conf_index < morris.conformers.Count && tdrd_conf_index < logprob.conformers.Count)
        //    {
        //        ConformerContainer<RegionCounts> morris_conf = morris.conformers[morris_conf_index];
        //        ConformerContainer<double> tdrd_conf = logprob.conformers[tdrd_conf_index];
        //        if (morris_conf.model == tdrd_conf.model)
        //        {
        //            sb.AppendLine($"#Model: {tdrd_conf.model.pdbModelId}");
        //            int morris_chain_index = 0;
        //            int tdrd_chain_index = 0;
        //            while (morris_chain_index < morris_conf.chains.Count && tdrd_chain_index < tdrd_conf.chains.Count)
        //            {
        //                ChainContainer<RegionCounts> morris_chain = morris_conf.chains[morris_chain_index];
        //                ChainContainer<double> tdrd_chain = tdrd_conf.chains[tdrd_chain_index];
        //                if (morris_chain.chain == tdrd_chain.chain)
        //                {
        //                    sb.AppendLine($"#Chain: {tdrd_chain.chain.pdbChainId}");
        //                    int morris_residue_index = 0;
        //                    int tdrd_residue_index = 0;
        //                    while (morris_residue_index < morris_chain.residues.Count && tdrd_residue_index < tdrd_chain.residues.Count)
        //                    {
        //                        ResidueContainer<RegionCounts> morris_residue = morris_chain.residues[morris_residue_index];
        //                        ResidueContainer<double> tdrd_residue = tdrd_chain.residues[tdrd_residue_index];
        //                        if (morris_residue.residue == tdrd_residue.residue)
        //                        {
        //                            string morris_class = $"{(morris_residue.data.core > 0 ? "C" : null)}{(morris_residue.data.allowed > 0 ? "A" : null)}{(morris_residue.data.generous > 0 ? "G" : null)}{(morris_residue.data.outside > 0 ? "O" : null)}";
        //                            sb.AppendLine($"Residue{tdrd_residue.residue.sequenceNumber}_{tdrd_residue.residue.name} {tdrd_residue.data.ToString(System.Globalization.CultureInfo.InvariantCulture)} {morris_class}");
        //                            morris_residue_index++;
        //                        }
        //                        else if (morris_residue.residue.index < tdrd_residue.residue.index)
        //                        {
        //                            morris_residue_index++;
        //                        }
        //                        else if (tdrd_residue.residue.index < morris_residue.residue.index)
        //                        {
        //                            tdrd_residue_index++;
        //                        }
        //                        else
        //                        {
        //                            throw new Exception("order assertion failed at residue level");
        //                        }

        //                    }
        //                    morris_chain_index++;
        //                }
        //                else if (morris_chain.chain.index < tdrd_chain.chain.index)
        //                {
        //                    morris_chain_index++;
        //                }
        //                else if (morris_chain.chain.index > tdrd_chain.chain.index)
        //                {
        //                    tdrd_chain_index++;
        //                }
        //                else
        //                {
        //                    throw new Exception("order assertion failed at chain level");
        //                }
        //            }
        //            morris_conf_index++;
        //        }
        //        else if (morris_conf.model.pdbModelId < tdrd_conf.model.pdbModelId)
        //        {
        //            morris_conf_index++;
        //        }
        //        else if (morris_conf.model.pdbModelId > tdrd_conf.model.pdbModelId)
        //        {
        //            tdrd_conf_index++;
        //        }
        //        else
        //        {
        //            throw new Exception("order assertion failed at conformer level");
        //        }
        //    }
        //    return sb;
        //}

        //public static EnsembleContainer<RegionCounts> classify_ensemble(EnsembleContainer<Vector2> dihedrals, Classifier c)
        //{



        //    EnsembleContainer<RegionCounts> rtn = new EnsembleContainer<RegionCounts>()
        //    {
        //        pdb = dihedrals.pdb,
        //        data = new RegionCounts(),
        //    };
        //    foreach (ConformerContainer<Vector2> conf in dihedrals.conformers)
        //    {
        //        ConformerContainer<RegionCounts> temp_conformer = new ConformerContainer<RegionCounts>()
        //        {
        //            model = conf.model,
        //            data = new RegionCounts(),
        //        };
        //        foreach (ChainContainer<Vector2> ca in conf.chains)
        //        {
        //            ChainContainer<RegionCounts> temp_chain = new ChainContainer<RegionCounts>()
        //            {
        //                chain = ca.chain,
        //                data = new RegionCounts(),
        //            };
        //            foreach (ResidueContainer<Vector2> ra in ca.residues.Where(r =>
        //                 !float.IsNaN(r.data.X)
        //                 && !float.IsNaN(r.data.Y)
        //                 && r.residue.AminoAcidType != Residue.AminoAcid.Pro
        //                 && r.residue.AminoAcidType != Residue.AminoAcid.Gly))
        //            //this filter removes the missing first and last residues, and GLY + PRO
        //            {
        //                RegionCounts q = new RegionCounts(c.classify(ra.data.X, ra.data.Y));
        //                temp_chain.residues.Add(new ResidueContainer<RegionCounts>()
        //                {
        //                    residue = ra.residue,
        //                    data = q
        //                });
        //                temp_chain.data += q;
        //            }
        //            temp_conformer.data += temp_chain.data;
        //            temp_conformer.chains.Add(temp_chain);
        //        }
        //        rtn.data += temp_conformer.data;
        //        rtn.conformers.Add(temp_conformer);
        //    }
        //    return rtn;
        //}

        //public static EnsembleContainer<RegionCounts> classify_ensemble2(EnsembleContainer<Vector2> dihedrals, Classifier c) =>
        //    aggregate_ensemle(transform_ensemble(dihedrals, inp => new RegionCounts(c.classify(inp.X, inp.Y))), inp => inp.Aggregate(new RegionCounts(), (acc, item) => acc + item));

        //public static EnsembleContainer<TResultData> transform_ensemble<TInputData, TResultData>(EnsembleContainer<TInputData> inputEnsemble, Func<TInputData, TResultData> transform)
        //    => new EnsembleContainer<TResultData>()
        //    {
        //        pdb = inputEnsemble.pdb,
        //        data = transform(inputEnsemble.data),
        //        conformers = inputEnsemble.conformers.Select(conf => new ConformerContainer<TResultData>()
        //        {
        //            model = conf.model,
        //            data = transform(conf.data),
        //            chains = conf.chains.Select(ch => new ChainContainer<TResultData>()
        //            {
        //                chain = ch.chain,
        //                data = transform(ch.data),
        //                residues = ch.residues.Select(res => new ResidueContainer<TResultData>()
        //                {
        //                    residue = res.residue,
        //                    data = transform(res.data)
        //                }).ToList()
        //            }).ToList()
        //        }).ToList()
        //    };

        //public static EnsembleContainer<T> aggregate_ensemle<T>(EnsembleContainer<T> inputEnsemble, Func<IEnumerable<T>, T> aggregator)
        //    => new EnsembleContainer<T>()
        //    {
        //        pdb = inputEnsemble.pdb,
        //        data = aggregator(inputEnsemble.conformers.Select(x => x.data)),
        //        conformers = inputEnsemble.conformers.Select(conf => new ConformerContainer<T>()
        //        {
        //            model = conf.model,
        //            data = aggregator(conf.chains.Select(x => x.data)),
        //            chains = conf.chains.Select(ch => new ChainContainer<T>()
        //            {
        //                chain = ch.chain,
        //                data = aggregator(ch.residues.Select(res => res.data)),
        //                residues = ch.residues
        //            }).ToList()
        //        }).ToList()
        //    };


        //public static EnsembleContainer<double> get_log_probability_for_ensemble(EnsembleContainer<Vector2> dihedrals, TDRD tdrd)
        //{
        //    EnsembleContainer<double> rtn = new EnsembleContainer<double>()
        //    {
        //        pdb = dihedrals.pdb
        //    };
        //    foreach (ConformerContainer<Vector2> conf in dihedrals.conformers)
        //    {
        //        ConformerContainer<double> cc = new ConformerContainer<double>() { model = conf.model };
        //        foreach (ChainContainer<Vector2> ca in conf.chains)
        //        {
        //            int count = 0;
        //            ChainContainer<double> cd = new ChainContainer<double>() { chain = ca.chain };
        //            foreach (ResidueContainer<Vector2> ra in ca.residues.Where(r =>
        //                 !double.IsNaN(r.data.X)
        //                 && !double.IsNaN(r.data.Y)
        //                ))
        //            {
        //                double d = tdrd.get_log_probability_for_residue(ra);
        //                cd.residues.Add(new ResidueContainer<double>() { residue = ra.residue, data = d });
        //                cd.data += d;
        //                count++;
        //            }
        //            cd.data /= count;
        //            cc.chains.Add(cd);
        //            cc.data += cd.data;
        //        }
        //        cc.data /= cc.chains.Count;
        //        rtn.conformers.Add(cc);
        //        rtn.data += cc.data;
        //    }
        //    rtn.data /= rtn.conformers.Count;
        //    return rtn;
        //}

        //public static EnsembleContainer<double> get_log_probability_for_ensemble(EnsembleContainer<Vector2> dihedrals, NDRD ndrd)
        //{
        //    EnsembleContainer<double> rtn = new EnsembleContainer<double>()
        //    {
        //        pdb = dihedrals.pdb
        //    };
        //    foreach (ConformerContainer<Vector2> conf in dihedrals.conformers)
        //    {
        //        ConformerContainer<double> cc = new ConformerContainer<double>() { model = conf.model };
        //        foreach (ChainContainer<Vector2> ca in conf.chains)
        //        {
        //            int count = 0;
        //            ChainContainer<double> cd = new ChainContainer<double>() { chain = ca.chain };
        //            foreach (ResidueContainer<Vector2> ra in ca.residues.Where(r =>
        //                 !double.IsNaN(r.data.X)
        //                 && !double.IsNaN(r.data.Y)
        //                ))
        //            {
        //                double d = ndrd.get_log_probability_for_residue(ra);
        //                cd.residues.Add(new ResidueContainer<double>() { residue = ra.residue, data = d });
        //                cd.data += d;
        //                count++;
        //            }
        //            cd.data /= count;
        //            cc.chains.Add(cd);
        //            cc.data += cd.data;
        //        }
        //        cc.data /= cc.chains.Count;
        //        rtn.conformers.Add(cc);
        //        rtn.data += cc.data;
        //    }
        //    rtn.data /= rtn.conformers.Count;
        //    return rtn;
        //}

        //public static EnsembleContainer<LogProbStats> get_log_prob_stats_for_ensemble(EnsembleContainer<double> logprob)
        //{
        //    //for debug
        //    IOrderedEnumerable<double> all = logprob.conformers.SelectMany(conf => conf.chains.SelectMany(ch => ch.residues.Select(res => res.data))).OrderBy(x => x);
        //    return new EnsembleContainer<LogProbStats>()
        //    {
        //        pdb = logprob.pdb,
        //        data = new LogProbStats(all, 10),
        //        conformers = logprob.conformers.Select(conf => new ConformerContainer<LogProbStats>()
        //        {
        //            model = conf.model,
        //            data = new LogProbStats(conf.chains.SelectMany(ch => ch.residues.Select(res => res.data)).OrderBy(x => x)),
        //            chains = conf.chains.Select(ch => new ChainContainer<LogProbStats>()
        //            {
        //                chain = ch.chain,
        //                data = new LogProbStats(ch.residues.Select(res => res.data).OrderBy(x => x)),
        //                residues = null,
        //            }).ToList(),
        //        }).ToList()
        //    };
        //}

    }
}
