﻿using PEV.Models.Ensemble;
using System;
using System.Linq;

namespace PEV.Calculations
{
    /// <summary>
    /// structured way to filter a pdb file
    /// </summary>
    public class EnsembleFilter
    {
        /// <summary>
        /// negate all filtering effect (eg, exclude al GLY and PRO residues)
        /// </summary>
        public bool negate_all = false;
        /// <summary>
        /// chain ID characters
        /// </summary>
        public string[] chainIds = null;
        /// <summary>
        /// model id numbers (as in the pdb file)
        /// </summary>
        public int[] modelIds = null;
        /// <summary>
        /// residue sequence numbers (same as pdb file)
        /// </summary>
        public int[] residueSequenceNumbers = null;
        /// <summary>
        /// residue types 3 letter notation
        /// </summary>
        public string[] residueTypes = null;

        public EnsembleFilter(char chainId = ' ', int modelId = -1, int residueSequenceNumber = -1, string residueType = null)
        {
            this.negate_all = false;
            this.chainIds = chainId == ' ' ? null : new string[] { chainId.ToString() };
            this.modelIds = modelId < 0 ? null : new int[] { modelId };
            this.residueSequenceNumbers = residueSequenceNumber < 0 ? null : new int[] { residueSequenceNumber };
            this.residueTypes = residueType == null ? null : new string[] { residueType };
        }

        public EnsembleContainerLayer<T> filter<T>(EnsembleContainerLayer<T> source) => new EnsembleContainerLayer<T>()
        {
            me = source.me,
            childs = (modelIds == null ? source.childs : source.childs.Where(conf => negate_all ^ modelIds.Contains(conf.me.model.pdbModelId)))
            .Select(conf => new ConformerContainerLayer<T>() {
                me = conf.me,
                childs = (chainIds == null ? conf.childs : conf.childs.Where(ch => negate_all ^ chainIds.Contains(ch.me.chain.pdbChainId.ToString())))
                .Select(ch => new ChainContainerLayer<T>()
                {
                    me = ch.me,
                    childs = (residueTypes == null ? ch.childs : ch.childs.Where(res => negate_all ^ residueTypes.Contains(res.residue.name, StringComparer.OrdinalIgnoreCase)))
                    .Where(res => residueSequenceNumbers == null || (negate_all ^ residueSequenceNumbers.Contains(res.residue.sequenceNumber))).ToList()
                }).ToList()
            }).ToList()
        };

        //public EnsembleContainer<T> filter<T>(EnsembleContainer<T> original) => new EnsembleContainer<T>()
        //{
        //    pdb = original.pdb,
        //    data = original.data,
        //    conformers = (modelIds == null ? original.conformers : original.conformers.Where(conf => negate_all ^ modelIds.Contains(conf.model.pdbModelId)))
        //    .Select(conf => new ConformerContainer<T>()
        //    {
        //        model = conf.model,
        //        data = conf.data,
        //        chains = (chainIds == null ? conf.chains : conf.chains.Where(ch => negate_all ^ chainIds.Contains(ch.chain.pdbChainId.ToString())))
        //        .Select(ch => new ChainContainer<T>()
        //        {
        //            chain = ch.chain,
        //            data = ch.data,
        //            residues = (residueTypes == null ? ch.residues : ch.residues.Where(res => negate_all ^ residueTypes.Contains(res.residue.name, StringComparer.OrdinalIgnoreCase)))
        //            .Where(res => residueSequenceNumbers == null || (negate_all ^ residueSequenceNumbers.Contains(res.residue.sequenceNumber))).ToList(),
        //        })
        //        .ToList()
        //    })
        //    .ToList()
        //};

    }
}
