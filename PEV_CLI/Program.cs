﻿using System;
using Microsoft.Extensions.CommandLineUtils;
using PEV.Calculations;
using PEV.Models.Pdb;
using Newtonsoft.Json;
using System.IO;
using PEV.Models.Ensemble;

namespace PEV_CLI
{
    class Program
    {
        static void Main(string[] args)
        {
            //PdbFun.getPdbFromId("1d3z", null, PdbFun.LoadingOptions.save_pdb).Wait();
            //var q1 = JsonConvert.DeserializeObject<Protein>(File.ReadAllText("1d3z.json"));
            //var q2 = q1.fix_uplinking_references();
            //var q3 = StaticCalculations.dihedral_angles(q2);
            //Console.WriteLine(q3);
            //Console.WriteLine(StaticCalculations.dihedral_angles(JsonConvert.DeserializeObject<MutableProtein>(File.ReadAllText("1d3z.json")).fix_uplinking_references()));

            //var w1 = JsonConvert.DeserializeObject<EnsembleFilter>(File.ReadAllText("filter1.json"));
            //var w2 = JsonConvert.DeserializeObject<EnsembleContainer<dynamic>>(File.ReadAllText("1d3z_dihedrals.json"));
            //var w3 = w1.filter(w2);
            //var w4 = JsonConvert.SerializeObject(JsonConvert.DeserializeObject<EnsembleFilter>(File.ReadAllText("filter1.json")).filter(
            //            JsonConvert.DeserializeObject<EnsembleContainer<dynamic>>(File.ReadAllText("1d3z_dihedrals.json"))));

            CommandLineApplication app = new CommandLineApplication()
            {
                Name = "pev"
            };
            app.HelpOption("-?|-h|--help");
            app.OnExecute(() =>
            {
                Console.WriteLine("Hello World!");
                return 0;
            });
            app.Command("fetchPdb", (command) =>
            {
                command.Description = "Downloads a PDB file from the RCSB database, unzips it, parses it and dumps it to stdout";
                command.HelpOption("-?|-h|--help");

                CommandArgument pdbIdArg = command.Argument("[pdbId]", "The ID of the pdb to fetch.", true);

                CommandOption JsonOption = command.Option("-f|--file", "Prints output to file. File name will be {pdbId}.json", CommandOptionType.NoValue);

                command.OnExecute(() =>
                {
                    foreach(string pdbId in pdbIdArg.Values)
                    {
                        string result = JsonConvert.SerializeObject(PdbFun.getPdbFromId(pdbId, null, PdbFun.LoadingOptions.save_pdb).Result);
                        if (JsonOption.Values.Count > 0)
                        {
                            File.WriteAllText($"{pdbId}.json", result);
                        }
                        else
                        {
                            Console.WriteLine(result);
                        }
                    }
                    return 0;
                });
            });
            app.Command("calcDihedrals", command =>
            {
                command.Description = "Calculate dihedral angles from parsed pdb.json. Returns an ensemble.json wich contains phi, psi data (as X,Y)";
                command.HelpOption("-?|-h|--help");

                CommandArgument inputFileArg = command.Argument("[inputFile]", "Input .json file", false);

                CommandOption outputFileOption = command.Option("-o|--out|--output", "Output json file", CommandOptionType.SingleValue);

                command.OnExecute(() => {
                    string result = JsonConvert.SerializeObject(EnsembleCalculations.dihedral_angles(JsonConvert.DeserializeObject<Protein>(File.ReadAllText(inputFileArg.Value)).fix_uplinking_references()));
                    if(outputFileOption.HasValue())
                    {
                        File.WriteAllText(outputFileOption.Value(), result);
                    }
                    else
                    {
                        Console.WriteLine(result);
                    }
                    return 0;
                });
            });

            app.Command("filterEnsemble", command =>
            {
                command.Description = "Filters an ensemble based on the given json filter. TODO more documentation. Returns the filtered ensemble.json ";
                command.HelpOption("-?|-h|--help");

                CommandArgument inputEnsembleArg = command.Argument("[inputEnsemble.json]", "Input ensemble.json", false);
                CommandArgument inputFilterArg = command.Argument("[inputFilter.json]", "Input filter.json", false);

                CommandOption outputFileOption = command.Option("-o|--out|--output", "Output json file", CommandOptionType.SingleValue);

                command.OnExecute(() => {
                    string result = JsonConvert.SerializeObject(JsonConvert.DeserializeObject<EnsembleFilter>(File.ReadAllText(inputFilterArg.Value)).filter(
                        JsonConvert.DeserializeObject<EnsembleContainer<dynamic>>(File.ReadAllText(inputEnsembleArg.Value))));
                    if (outputFileOption.HasValue())
                    {
                        File.WriteAllText(outputFileOption.Value(), result);
                    }
                    else
                    {
                        Console.WriteLine(result);
                    }
                    return 0;
                });
            });


            app.Execute(args);
        }
    }
}