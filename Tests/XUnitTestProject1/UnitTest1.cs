using System;
using System.Linq;
using System.Collections.Generic;
using System.IO;
using Xunit;

using PEV.Models;
using PEV.Calculations;
using PEV.Calculations.Ting2010;
using PEV.Models.Pdb;
using PEV.Models.Ensemble;

namespace XUnitTestProject1
{
    public class UnitTest1
    {
        [Fact]
        public void TestPdbRead_1d3z()
        {
            ProteinLayer pdb_1d3z = PdbFun.getPdbFromId("1d3z").Result;
            Assert.True(pdb_1d3z.me.title.Contains("UBIQUITIN NMR STRUCTURE"));
            Assert.Equal(10, pdb_1d3z.models.Count);
            Assert.Equal("ALA", pdb_1d3z.childs[0].childs[45].me.name);
            Assert.Equal(2.682f, pdb_1d3z.models[1].childs[pdb_1d3z.childs[0].childs[45].atomMap["N"].dataIndex].coordinates.Z);
        }

        //[Fact]
        //public void TestCalcDihedralAngles()
        //{
        //    Protein pdb_1d3z = Protein.fromPdbId("1d3z").Result;

        //    EnsembleAngles e_1d3z = new EnsembleAngles(pdb_1d3z);
        //    Assert.Equal(50.00253, e_1d3z.conformers[0].chains[0].residues[45].phi, 4);
        //}

        //[Fact]
        //public void TestClassification()
        //{
        //    Protein pdb_1d3z = Protein.fromPdbId("1d3z").Result;
        //    EnsembleAngles e_1d3z = new EnsembleAngles(pdb_1d3z);

        //    PEV.Calculations.Morris1992.Classifier classifier = new PEV.Calculations.Morris1992.Classifier();
        //    classifier.load_config(@"E:\repos\PEV.NET\PEV.NET\ramachandran_classification.json");

        //    Dictionary<char, long> sum = new Dictionary<char, long>() { {'C', 0}, {'A', 0}, {'G', 0}, {' ', 0} };
        //    foreach(ConformerAngles conf in e_1d3z.conformers)
        //    {
        //        foreach(ChainAngles ca in conf.chains)
        //        {
        //            foreach(ResidueAngle ra in ca.residues.Where(r=>
        //                !double.IsNaN(r.phi) 
        //                && !double.IsNaN(r.psi) 
        //                && r.residue.name != "GLY" 
        //                && r.residue.name != "PRO"))
        //            {
        //                sum[classifier.classify(ra.phi, ra.psi)]++;
        //            }
        //        }
        //    }
        //    Assert.Equal(638, sum['C']);
        //    Assert.Equal(22, sum['A']);
        //    Assert.Equal(0, sum['G']);
        //    Assert.Equal(0, sum[' ']);
        //}

        //[Fact]
        //public void Calc_residue_names()
        //{
        //    int n = 0;
        //    System.Diagnostics.Debug.WriteLine((Enum.GetNames(typeof(Residue.AminoAcid)).Aggregate("", (a, i) => $"{a}\"{i.ToUpper()}\" : {n++},")));
        //}

        //[Fact]
        //public void test_downloadPdb_normal()
        //{
        //    Protein p = Protein.fromPdbId("1d3z").Result;
        //    EnsembleAngles e = new EnsembleAngles(p);
        //    Protein p2 = new Protein();
        //    p2.parse_pdb_lines(File.ReadAllLines(@"D:\repos\PEV.NET\PEV.NET\1d3z.pdb"));

        //}

        //[Fact]
        //public void test_ndrd_loading()
        //{
        //    NDRD_Ting2010 ndrd = new NDRD_Ting2010()
        //    {
        //        neighboor_type = "left",
        //    };
        //    ndrd.load_config(@"D:\repos\PEV.NET\PEV.NET\NDRD\NDRD_TCBIG.txt");

        //    Assert.Equal(new ConfigStructure() { probablity = 3.658695e-05f, log_probability = 10.21582f, cumulative_sum = 3.658695e-05f }, ndrd.matrix[0][0][0][0]);

        //    Assert.Equal(1, ndrd.matrix[0][0].Sum((row) => row.Sum(s => s.probablity)));

        //}

        //[Fact]
        //public void test_tdrd_loading()
        //{
        //    TDRD_Ting2010 tdrd = new TDRD_Ting2010()
        //    {
        //        neighboor_type = "left"
        //    };
        //    tdrd.load_config(@"D:\repos\PEV.NET\PEV.NET\NDRD\NDRD_TCBIG.txt");
        //    Assert.Equal(new ConfigStructure() { probablity = 3.327240e-05f, log_probability = 10.31078f, cumulative_sum = 3.327240e-05f }, tdrd.matrix[0][0][0]);
        //    Assert.Equal(1, tdrd.matrix[0].Sum((row) => row.Sum(s => s.probablity)));

        //    System.Threading.Tasks.Task<Protein> pTask = Protein.fromPdbId("1d3z", forced_overwrite: true);
        //    pTask.Wait();
        //    Protein p = pTask.Result;
        //    EnsembleAngles e = new EnsembleAngles(p);

        //    List<List<List<double>>> results = new List<List<List<double>>>();
        //    foreach(ConformerAngles conf in e.conformers)
        //    {
        //        results.Add(new List<List<double>>());
        //        foreach(ChainAngles ca in conf.chains)
        //        {
        //            results.Last().Add(new List<double>());
        //            foreach(ResidueAngle r in ca.residues.Where(x=>!double.IsNaN(x.phi) && !double.IsNaN(x.psi) ) )
        //            {
        //                results.Last().Last().Add(tdrd.get_distribution(r).probablity);
        //            }
        //        }
        //    }
        //    File.WriteAllLines("test2.data", results[0][0].Select(f => f.ToString()));
        //}

    }
}
