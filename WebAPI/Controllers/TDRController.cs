﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;

using PEV.Calculations.Ting2010;
using PEV.Calculations;
using PEV.Models.Ensemble;
using PEV.Models.Pdb;
using System.Numerics;
using System.IO;

namespace WebAPI.Controllers
{
    /// <summary>
    /// Type Dependent Ramachandran
    /// </summary>
    [Route("[controller]/[action]")]
    public class TDRController : Controller
    {
        /// <summary>
        /// Type Dependent Ramachandran Distribution Data Set
        /// </summary>
        [Flags]
        public enum TDRDDataSet : long
        {
            LeftNeighboor = 1 << 1,
            RightNeighboor = 1 << 2,
            TCBIG = 1 << 11,
            TCB = 1 << 12,
            T = 1 << 13,
            C = 1 << 14,
            //combinations
            TDRD_L_TCBIG = LeftNeighboor | TCBIG,
            TDRD_R_TCBIG = RightNeighboor | TCBIG,
            TDRD_R_TCB = RightNeighboor | TCB,
            TDRD_L_TCB = LeftNeighboor | TCB,
            TDRD_L_T = LeftNeighboor | T,
            TDRD_R_T = RightNeighboor | T,
            TDRD_R_C = RightNeighboor | C,
            TDRD_L_C = LeftNeighboor | C,
        }

        /// <summary>
        /// collection of type dependent 
        /// </summary>
        public static Dictionary<TDRDDataSet, TDRD> configs_TDRD = new Dictionary<TDRDDataSet, TDRD>();
       // = Enum.GetValues(typeof(TDRDDataSet))
       //.Cast<long>().Where(x => !PEV.CommrutLib.isFlag(x)).Cast<TDRDDataSet>()
       //.ToDictionary(x => x, x => TDRD.fromJson(Path.Combine(TestsController.basePath, "NDRD", $"{x.ToString()}.json")));

        public static TDRDDataSet get_dataset_id(
            string dataset = nameof(TDRDDataSet.TCBIG),
            bool right_neighboor = false)
        {
            return (Enum.TryParse(dataset, out TDRDDataSet x) ? x : throw new ArgumentOutOfRangeException(nameof(dataset)))
                    | (right_neighboor ? TDRDDataSet.RightNeighboor : TDRDDataSet.LeftNeighboor);
        }

        public static TDRD get_config(TDRDDataSet id)
        {
            if(!configs_TDRD.ContainsKey(id))
            {
                configs_TDRD[id] = TDRD.fromJson(Path.Combine(TestsController.basePath, "NDRD", $"{id.ToString()}.json"));
            }
            return configs_TDRD[id];
        }

        public static TDRD get_config(string dataset = nameof(TDRDDataSet.TCBIG),
            bool right_neighboor = false) => get_config(get_dataset_id(dataset, right_neighboor));

        /// <summary>
        /// log Probability avarege for the ensemble, each conformer, each chain, each residue
        /// using type dependent ramachandran distribution 
        /// from on Ting et al 2010 [https://doi.org/10.1371/journal.pcbi.1000763]
        /// (ALL distribution in the article)
        /// </summary>
        /// <remarks>
        /// log Probability avarege for the ensemble, each conformer, each chain, each residue
        /// please note that the first and last residues are removed (due to missing dihedral angle)
        /// log = ln = log_e natural logaritm
        /// </remarks>
        /// <param name="id">PDB id</param>
        /// <param name="dataset">Name of dataset (possible values: 'TCBIG', 'TCB', 'T', 'C'), default: 'TCBIG'</param>
        /// <param name="right_neighboor">use right neighboor if true, left if false. default:false</param>
        /// <param name="chainId">restrict to a specific chain, leave empty (or use ' ') for an avg of all chains</param>
        /// <param name="modelId">restrict to a specific model, leave empty (or use -1) for an avg of all models</param>
        /// <param name="residueSequenceNumber">restrict to a specific resudie based on PDB sequence number, leave empty (or use -1) for an avg of residues</param>
        /// <param name="residueType">restrict to a specific retidue type ('name' according to PDB specifications). 
        /// Leave it empty (or use null) for a sum of all residue types</param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public EnsembleDto<double> logProbability(string id,
            string dataset = nameof(TDRDDataSet.TCBIG),
            bool right_neighboor = false,
            char chainId = ' ',
            int modelId = -1,
            int residueSequenceNumber = -1,
            string residueType = null)
        {
            TDRD t = get_config(get_dataset_id(dataset, right_neighboor));
            ProteinLayer p = PdbFun.getPdbFromId(id, TestsController.pdbPath, PdbFun.LoadingOptions.from_cache).Result;
            EnsembleContainerLayer<Vector2> dihedrals = EnsembleCalculations.dihedral_angles(p);
            EnsembleFilter filter = new EnsembleFilter(chainId, modelId, residueSequenceNumber, residueType);
            return EnsembleCalculations.get_log_prob_for_ensemble(filter.filter(dihedrals), t);
        }

        /// <summary>
        /// log Probability statistics for the ensemble, each conformer and each chain; 
        /// using type dependent ramachandran distribution
        /// </summary>
        /// <remarks>
        /// log Probability avarege for the ensemble, each conformer and each chain
        /// using type dependent ramachandran distribution 
        /// from Ting et al 2010 [https://doi.org/10.1371/journal.pcbi.1000763]
        /// (ALL distribution in the article) 
        /// please note that the first and last residues are removed (due to missing dihedral angle)
        /// log = ln = log_e natural logaritm
        /// </remarks>
        /// <param name="id">PDB id</param>
        /// <param name="dataset">Name of dataset (possible values: 'TCBIG', 'TCB', 'T', 'C'), default: 'TCBIG'</param>
        /// <param name="right_neighboor">use right neighboor if true, left if false. default:false</param>
        /// <param name="chainId">restrict to a specific chain, leave empty (or use ' ') for an avg of all chains</param>
        /// <param name="modelId">restrict to a specific model, leave empty (or use -1) for an avg of all models</param>
        /// <param name="residueSequenceNumber">restrict to a specific resudie based on PDB sequence number, leave empty (or use -1) for an avg of residues</param>
        /// <param name="residueType">restrict to a specific retidue type ('name' according to PDB specifications). 
        /// Leave it empty (or use null) for a sum of all residue types</param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public EnsembleDto<LogProbStats> logProbStats(string id,
            string dataset = nameof(TDRDDataSet.TCBIG),
            bool right_neighboor = false,
            char chainId = ' ',
            int modelId = -1,
            int residueSequenceNumber = -1,
            string residueType = null)
        {
            TDRD t = get_config(get_dataset_id(dataset, right_neighboor));
            ProteinLayer p = PdbFun.getPdbFromId(id, TestsController.pdbPath, PdbFun.LoadingOptions.from_cache).Result;
            EnsembleFilter filter = new EnsembleFilter(chainId, modelId, residueSequenceNumber, residueType);
            EnsembleContainerLayer<Vector2> dihedrals = EnsembleCalculations.dihedral_angles(p);
            return EnsembleCalculations.get_log_prob_stats(EnsembleCalculations.get_log_prob_for_ensemble(filter.filter(dihedrals), t));
        }
    }
}
