﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PEV.Models;
using PEV.Models.Ensemble;
using PEV.Models.Pdb;
using PEV.Calculations;
using PEV.Calculations.Morris1992;
using System.Numerics;
using PEV.Calculations.Ting2010;
using System.IO;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WebAPI.Controllers
{
    /// <summary>
    /// controller to generate gnuplot input data
    /// </summary>
    [Route("[controller]/[action]")]
    public class PlotController : Controller
    {
        /// <summary>
        /// plain text phi, psi, residueType values for gnuplot. save it to a file for best results (:
        /// </summary>
        /// <param name="id">PDB id</param>
        /// <param name="chainId">restrict to a specific chain, leave empty (or use ' ') for a sum of all chains</param>
        /// <param name="modelId">restrict to a specific model, leave empty (or use -1) for a sum of all models</param>
        /// <param name="residueSequenceNumber">restrict to a specific resudie based on PDB sequence number, leave empty (or use -1) for a sum of all residues</param>
        /// <param name="residueType">restrict to a specific retidue type ('name' according to PDB specifications), leave it empty (or use null) for a sum of all residue types</param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public string RamachandranPlot(string id, char chainId = ' ', int modelId = -1,
            int residueSequenceNumber = -1, string residueType = null)
        {

            EnsembleContainerLayer<Vector2> dihedrals = EnsembleCalculations.dihedral_angles(PdbFun.getPdbFromId(id, TestsController.pdbPath, PdbFun.LoadingOptions.from_cache).Result);
            EnsembleFilter filter = new EnsembleFilter(chainId, modelId, residueSequenceNumber, residueType);
            dihedrals = filter.filter(dihedrals);



            StringBuilder sb = new StringBuilder();
            sb.AppendLine("#phi psi residueType");
            foreach(string t in EnsembleCalculations.plot_ramachandran(dihedrals))
            {
                sb.AppendLine(t);
            }
            return sb.ToString();
        }

        /// <summary>
        /// plain text phi, psi, residueType values for gnuplot. save it to a file for best results (:
        /// </summary>
        /// <param name="id">PDB id</param>
        /// <param name="chainId">restrict to a specific chain, leave empty (or use ' ') for a sum of all chains</param>
        /// <param name="modelId">restrict to a specific model, leave empty (or use -1) for a sum of all models</param>
        /// <param name="residueSequenceNumber">restrict to a specific resudie based on PDB sequence number, leave empty (or use -1) for a sum of all residues</param>
        /// <param name="residueType">restrict to a specific retidue type ('name' according to PDB specifications), leave it empty (or use null) for a sum of all residue types</param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public IActionResult RamachandranPlotStream(string id, char chainId = ' ', int modelId = -1,
            int residueSequenceNumber = -1, string residueType = null)
        {

            EnsembleContainerLayer<Vector2> dihedrals = EnsembleCalculations.dihedral_angles(PdbFun.getPdbFromId(id, TestsController.pdbPath, PdbFun.LoadingOptions.from_cache).Result);
            EnsembleFilter filter = new EnsembleFilter(chainId, modelId, residueSequenceNumber, residueType);
            dihedrals = filter.filter(dihedrals);
            var stream = new MemoryStream();
            var sw = new StreamWriter(stream, Encoding.UTF8);
            foreach(var s in EnsembleCalculations.plot_ramachandran(dihedrals))
            {
                sw.WriteLine(s);
            }
            stream.Position = 0;
            return File(stream, "text/plain", $"{id}_ramachandran.plot");

        }

        //[HttpGet("{id}")]
        //public string PlotMorris1992Classes(string id, char chainId = ' ', int modelId = -1,
        //    int residueSequenceNumber = -1, string residueType = null)
        //{
        //    StringBuilder sb = new StringBuilder();
        //    Protein p = Protein.fromPdbId(id, TestsController.pdbPath, save_pdb: false).Result;
        //    EnsembleContainer<Vector2> dihedrals = StaticCalculations.dihedral_angles(p);
        //    EnsembleFilter filter = new EnsembleFilter(chainId, modelId, residueSequenceNumber, residueType);
        //    EnsembleContainer<RegionCounts> c = TraditionalRamachandranController.classifier.classify(filter.filter(dihedrals));
        //    foreach(ConformerContainer<RegionCounts> conf in c.conformers)
        //    {
        //        foreach(ChainContainer<RegionCounts> chain in conf.chains)
        //        {
        //            foreach(ResidueContainer<RegionCounts> r in chain.residues)
        //            {
        //                sb.AppendLine($"{(r.data.core > 0 ? "C" : null)}{(r.data.allowed > 0 ? "A" : null)}{(r.data.generous > 0 ? "G" : null)}{(r.data.outside > 0 ? "O" : null)}");
        //            }
        //        }
        //    }
        //    return sb.ToString();
        //}

        /// <summary>
        /// Calculates both traditional ramachandran and Type (or neighboor) dependent Ramachandran log probability for a PDB and returns this in clear text for plotting
        /// </summary>
        /// <remarks>
        /// <p>
        /// Traditional Ramachandran classes are based on Morris et al 1992 [https://doi.org/10.1002/prot.340120407], where GLY and PRO residues are removed. 
        /// </p><p>
        /// Type (or Neighboor) dependent ramachandran probability is based on Ting et al 2010 [https://doi.org/10.1371/journal.pcbi.1000763]
        /// </p>
        /// </remarks>
        /// <param name="id">PDB id</param>
        /// <param name="neighboor_dependent">false to use (only) Type Dependent Ramachandran Distribution, true to use Neighboor Dependent Ramachandran Distribution</param>
        /// <param name="dataset">Name of dataset (possible values: 'TCBIG', 'TCB', 'T', 'C')</param>
        /// <param name="right_neighboor">use right neighboor if true, left if false</param>
        /// <param name="chainId">restrict to a specific chain, leave empty (or use ' ') for an avg of all chains</param>
        /// <param name="modelId">restrict to a specific model, leave empty (or use -1) for an avg of all models</param>
        /// <param name="residueSequenceNumber">restrict to a specific resudie based on PDB sequence number, leave empty (or use -1) for an avg of residues</param>
        /// <param name="residueType">restrict to a specific retidue type ('name' according to PDB specifications). 
        /// Leave it empty (or use null) for a sum of all residue types</param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public string PlotMorrisClassVsTinglogProb(string id, bool neighboor_dependent = false, string dataset = "TCBIG", bool right_neighboor = false, char chainId = ' ', int modelId = -1, int residueSequenceNumber = -1, string residueType = null)
        {
            
            ProteinLayer p = PdbFun.getPdbFromId(id, TestsController.pdbPath, PdbFun.LoadingOptions.from_cache).Result;
            EnsembleContainerLayer<Vector2> dihedrals = EnsembleCalculations.dihedral_angles(p);
            EnsembleFilter filter = new EnsembleFilter(chainId, modelId, residueSequenceNumber, residueType);
            EnsembleContainerLayer<Vector2> filtered_dihedrals = filter.filter(dihedrals);
            EnsembleContainerLayer<RegionCounts> morris_pdb = EnsembleCalculations.classify_ensemble(filtered_dihedrals, TraditionalRamachandranController.classifier);
            EnsembleContainerLayer<double> logprob;
            if (neighboor_dependent)
            {
                NDRD n = NDRController.get_config(NDRController.get_dataset_id(dataset, right_neighboor));
                logprob = filter.filter(EnsembleCalculations.get_log_prob_for_ensemble (dihedrals, n)); //bcs we are neighboor dependent we must filter after neighboor dependent calculations are done.
            }
            else
            {
                TDRD t = TDRController.get_config(TDRController.get_dataset_id(dataset, right_neighboor));
                logprob = EnsembleCalculations.get_log_prob_for_ensemble(filtered_dihedrals, t); //bcs its only type dependent we can apply a filter before calculation to speed things up
            }
            EnsembleContainerLayer<string> q = morris_pdb.zip(logprob, (x, y) => $" {y.ToString(System.Globalization.CultureInfo.InvariantCulture)} {x.printSingle()}");

            return EnsembleCalculations.plot_data_with_id(q).Aggregate("", (acc, item) => acc + item + Environment.NewLine);
        }

        ///// <summary>
        ///// Calculates both traditional ramachandran and Type (or neighboor) dependent Ramachandran log probability for a PDB and returns this in clear text for plotting
        ///// </summary>
        ///// <remarks>
        ///// <p>
        ///// Traditional Ramachandran classes are based on Morris et al 1992 [https://doi.org/10.1002/prot.340120407], where GLY and PRO residues are removed. 
        ///// </p><p>
        ///// Type (or Neighboor) dependent ramachandran probability is based on Ting et al 2010 [https://doi.org/10.1371/journal.pcbi.1000763]
        ///// </p>
        ///// </remarks>
        ///// <param name="id">PDB id</param>
        ///// <param name="neighboor_dependent">false to use (only) Type Dependent Ramachandran Distribution, true to use Neighboor Dependent Ramachandran Distribution</param>
        ///// <param name="dataset">Name of dataset (possible values: 'TCBIG', 'TCB', 'T', 'C')</param>
        ///// <param name="right_neighboor">use right neighboor if true, left if false</param>
        ///// <param name="chainId">restrict to a specific chain, leave empty (or use ' ') for an avg of all chains</param>
        ///// <param name="modelId">restrict to a specific model, leave empty (or use -1) for an avg of all models</param>
        ///// <param name="residueSequenceNumber">restrict to a specific resudie based on PDB sequence number, leave empty (or use -1) for an avg of residues</param>
        ///// <param name="residueType">restrict to a specific retidue type ('name' according to PDB specifications). 
        ///// Leave it empty (or use null) for a sum of all residue types</param>
        ///// <returns></returns>
        //[HttpGet("{id}")]
        //public IActionResult PlotMorrisClassVsTinglogProbStream(string id, bool neighboor_dependent = false, string dataset = "TCBIG", bool right_neighboor = false, char chainId = ' ', int modelId = -1, int residueSequenceNumber = -1, string residueType = null)
        //{

        //    ProteinLayer p = PdbFun.getPdbFromId(id, TestsController.pdbPath, PdbFun.LoadingOptions.from_cache).Result;
        //    EnsembleContainerLayer<Vector2> dihedrals = EnsembleCalculations.dihedral_angles(p);
        //    EnsembleFilter filter = new EnsembleFilter(chainId, modelId, residueSequenceNumber, residueType);
        //    EnsembleContainerLayer<Vector2> filtered_dihedrals = filter.filter(dihedrals);
        //    EnsembleContainerLayer<RegionCounts> morris_pdb = EnsembleCalculations.classify_ensemble(filtered_dihedrals, TraditionalRamachandranController.classifier);
        //    EnsembleContainerLayer<double> logprob;
        //    if (neighboor_dependent)
        //    {
        //        NDRD n = NDRController.get_config(NDRController.get_dataset_id(dataset, right_neighboor));
        //        logprob = filter.filter(EnsembleCalculations.get_log_prob_for_ensemble(dihedrals, n)); //bcs we are neighboor dependent we must filter after neighboor dependent calculations are done.
        //    }
        //    else
        //    {
        //        TDRD t = TDRController.get_config(TDRController.get_dataset_id(dataset, right_neighboor));
        //        logprob = EnsembleCalculations.get_log_prob_for_ensemble(filtered_dihedrals, t); //bcs its only type dependent we can apply a filter before calculation to speed things up
        //    }
        //    EnsembleContainerLayer<string> q = morris_pdb.zip(logprob, (x, y) => $" {y.ToString(System.Globalization.CultureInfo.InvariantCulture)} {x.printSingle()}");
        //    var stream = new MemoryStream();
        //    var sw = new StreamWriter(stream, Encoding.UTF8);
        //    foreach(var s in EnsembleCalculations.plot_data_with_id(q))
        //    {
        //        sw.WriteLine(s);
        //    }
        //    return File(stream, "text/plain");
        //}

    }
}
