﻿using Microsoft.AspNetCore.Mvc;
using PEV.Calculations;
using PEV.Calculations.Ting2010;
using PEV.Models.Ensemble;
using PEV.Models.Pdb;
using System.Numerics;

namespace WebAPI.Controllers
{

    /// <summary>
    /// Neighboor Dependent Ramachandran calculations
    /// </summary>
    [Route("[controller]/[action]")]
    public partial class NDRController: Controller
    {

        /// <summary>
        /// log Probability avarege for the ensemble, each conformer, each chain, each residue
        /// using neighboor dependent ramachandran distribution
        /// from Ting et al 2010 [https://doi.org/10.1371/journal.pcbi.1000763]
        /// </summary>
        /// <remarks>
        /// log Probability avarege for the ensemble, each conformer, each chain, each residue
        /// please note that the first and last residues are removed (due to missing dihedral angle)
        /// </remarks>
        /// <param name="id">PDB id</param>
        /// <param name="dataset">Name of dataset (possible values: 'TCBIG', 'TCB', 'T', 'C')</param>
        /// <param name="right_neighboor">use right neighboor if true, left if false</param>
        /// <param name="chainId">restrict to a specific chain, leave empty (or use ' ') for an avg of all chains</param>
        /// <param name="modelId">restrict to a specific model, leave empty (or use -1) for an avg of all models</param>
        /// <param name="residueSequenceNumber">restrict to a specific resudie based on PDB sequence number, leave empty (or use -1) for an avg of residues</param>
        /// <param name="residueType">restrict to a specific retidue type ('name' according to PDB specifications). 
        /// Leave it empty (or use null) for a sum of all residue types</param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public EnsembleDto<double> logProbability(string id,
            string dataset = nameof(NDRDDataSet.TCBIG),
            bool right_neighboor = false,
            char chainId = ' ',
            int modelId = -1,
            int residueSequenceNumber = -1,
            string residueType = null)
        {
            NDRD n = get_config(dataset, right_neighboor);
            ProteinLayer p = PdbFun.getPdbFromId(id, TestsController.pdbPath, PdbFun.LoadingOptions.from_cache).Result;
            EnsembleContainerLayer<Vector2> dihedrals = EnsembleCalculations.dihedral_angles(p);
            EnsembleFilter filter = new EnsembleFilter(chainId, modelId, residueSequenceNumber, residueType);
            return EnsembleCalculations.get_log_prob_for_ensemble(filter.filter(dihedrals), n);
        }

        /// <summary>
        /// log Probability statistics for the ensemble, each conformer and each chain; 
        /// using neighboor dependent ramachandran distribution
        /// </summary>
        /// <remarks>
        /// log Probability avarege for the ensemble, each conformer and each chain
        /// using neighboor dependent ramachandran distribution 
        /// from Ting et al 2010 [https://doi.org/10.1371/journal.pcbi.1000763]
        /// (ALL distribution in the article) 
        /// please note that the first and last residues are removed (due to missing dihedral angle)
        /// log = ln = log_e natural logaritm
        /// </remarks>
        /// <param name="id">PDB id</param>
        /// <param name="dataset">Name of dataset (possible values: 'TCBIG', 'TCB', 'T', 'C'), default: 'TCBIG'</param>
        /// <param name="right_neighboor">use right neighboor if true, left if false. default:false</param>
        /// <param name="chainId">restrict to a specific chain, leave empty (or use ' ') for an avg of all chains</param>
        /// <param name="modelId">restrict to a specific model, leave empty (or use -1) for an avg of all models</param>
        /// <param name="residueSequenceNumber">restrict to a specific resudie based on PDB sequence number, leave empty (or use -1) for an avg of residues</param>
        /// <param name="residueType">restrict to a specific retidue type ('name' according to PDB specifications). 
        /// Leave it empty (or use null) for a sum of all residue types</param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public EnsembleDto<LogProbStats> logProbStats(string id,
            string dataset = nameof(NDRDDataSet.TCBIG),
            bool right_neighboor = false,
            char chainId = ' ',
            int modelId = -1,
            int residueSequenceNumber = -1,
            string residueType = null)
        {
            NDRD rd = get_config(get_dataset_id(dataset, right_neighboor));
            ProteinLayer p = PdbFun.getPdbFromId(id, TestsController.pdbPath, PdbFun.LoadingOptions.from_cache).Result;
            EnsembleFilter filter = new EnsembleFilter(chainId, modelId, residueSequenceNumber, residueType);
            EnsembleContainerLayer<Vector2> dihedrals = EnsembleCalculations.dihedral_angles(p);
            return EnsembleCalculations.get_log_prob_stats(EnsembleCalculations.get_log_prob_for_ensemble(filter.filter(dihedrals), rd));
        }

    }
}
