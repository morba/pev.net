﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Numerics;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Hosting;

using PEV.Models;
using PEV.Calculations;
using PEV.Calculations.Morris1992;
using PEV.Models.Ensemble;
using PEV.Models.Pdb;


namespace WebAPI.Controllers
{
    /// <summary>
    /// Classification of backbone dihedral angles based on Morris et al. 1992 [DOI: 10.1002/prot.340120407]
    /// </summary>
    [Route("[controller]/[action]")]
    public class TraditionalRamachandranController : Controller
    {
        //private IHostingEnvironment henv { get; }
        ///// <summary>
        ///// initialize the controller with the current hosting environment
        ///// </summary>
        ///// <param name="env"></param>
        //public TraditionalRamachandranController(IHostingEnvironment env) => henv = env;

        /// <summary>
        /// static classifier config
        /// </summary>
        public static readonly Classifier classifier
            = new Classifier(Path.Combine(TestsController.basePath, "wwwroot", "ramachandran_classification.json"));


        ///// <summary>
        ///// Summary of Ramachandran classification for the whole protein based on Morris et al 1992. [DOI: 10.1002/prot.340120407]
        ///// </summary>
        ///// <remarks>
        ///// Ramachandran classification for the protein based on Morris et al 1992. [DOI: 10.1002/prot.340120407]
        ///// Returns a single count of core, allowed, generous and outside residues
        ///// Please note that the first and last residue are removed (due to missing psi/phi angle), 
        ///// GLY and PRO residues are also removed (due to strong type dependence).
        ///// </remarks>
        ///// <param name="id">PDB id</param>
        ///// <param name="chainId">restrict to a specific chain, leave empty (or use ' ') for a sum of all chains</param>
        ///// <param name="modelId">restrict to a specific model, leave empty (or use -1) for a sum of all models</param>
        ///// <param name="residueSequenceNumber">restrict to a specific resudie based on PDB sequence number, leave empty (or use -1) for a sum of all residues</param>
        ///// <param name="residueType">restrict to a specific retidue type ('name' according to PDB specifications). 
        ///// Note that this overrides the default of leaving out GLY and PRO residues. 
        ///// Leave it empty (or use null) for a sum of all residue types</param>
        ///// <returns>count of residues in the regions: Core, Allowed, Generous and Outside</returns>
        //[HttpGet("{id}")]
        //public RegionCounts SumClassify(string id, char chainId = ' ', int modelId = -1,
        //    long residueSequenceNumber = -1, string residueType = null)
        //{
        //    //RamachandranClassifier_Morris1992 classifier = new RamachandranClassifier_Morris1992();
        //    //classifier.load_config(Path.Combine(henv.WebRootPath, "ramachandran_classification.json"));
        //    EnsembleAngles e_1d3z = new EnsembleAngles(Protein.fromPdbId(id, TestsController.pdbPath, save_pdb: false).Result);

        //    //Dictionary<char, long> sum = new Dictionary<char, long>() { { 'C', 0 }, { 'A', 0 }, { 'G', 0 }, { ' ', 0 } };
        //    RegionCounts rtn = new RegionCounts();
        //    foreach(ConformerAngles conf in e_1d3z.conformers.Where(c=>modelId < 0 ? true : c.pdb_model.pdbModelId == modelId))
        //    {
        //        foreach(ChainAngles ca in conf.chains.Where(c=>chainId == ' ' ? true : c.chain.pdbChainId == chainId))
        //        {
        //            foreach(ResidueAngle ra in ca.residues.Where(r =>
        //                !double.IsNaN(r.phi)
        //                && !double.IsNaN(r.psi)
        //                && (residueType == null 
        //                    ? (r.residue.name != "GLY" && r.residue.name != "PRO")
        //                    : r.residue.name == residueType)
        //                && (residueSequenceNumber < 0 || r.residue.sequenceNumber == residueSequenceNumber)
        //                ))
        //            {
        //                rtn.add(classifier.classify(ra.phi, ra.psi));
        //            }
        //        }
        //    }
        //    return rtn;
        //}

        /// <summary>
        /// Detailed Ramachandran classification for the protein backbone dihedral angles based on Morris et al 1992. [DOI: 10.1002/prot.340120407]
        /// </summary>
        /// <remarks>
        /// Ramachandran classification for the protein backbone dihedral angles based on Morris et al 1992. [DOI: 10.1002/prot.340120407]
        /// Returns a count for core, allowed, generous and outside residues grouped at varius "levels":
        /// a total sum, per model, per chain, per residue (the last one only contains a single '1' value)
        /// Please note that the first and last residue are removed (due to missing psi/phi angle).
        /// GLY and PRO residues are also removed (due to strong type dependence).
        /// </remarks>
        /// <param name="id">PDB id</param>
        /// <param name="chainId">restrict to a specific chain, leave empty (or use ' ') for a sum of all chains</param>
        /// <param name="modelId">restrict to a specific model, leave empty (or use -1) for a sum of all models</param>
        /// <param name="residueSequenceNumber">restrict to a specific resudie based on PDB sequence number, leave empty (or use -1) for a sum of all residues</param>
        /// <param name="residueType">restrict to a specific retidue type ('name' according to PDB specifications). 
        /// Note that this overrides the default of leaving out GLY and PRO residues. 
        /// Leave it empty (or use null) for a sum of all residue types</param>
        /// <returns>count of residues in the regions: Core, Allowed, Generous and Outside</returns>
        [HttpGet("{id}")]
        public EnsembleDto<RegionCounts> Classify(string id, char chainId = ' ', int modelId = -1, int residueSequenceNumber = -1, string residueType = null)
        {
            //RamachandranClassifier_Morris1992 classifier = new RamachandranClassifier_Morris1992();
            //classifier.load_config(Path.Combine(henv.WebRootPath, "ramachandran_classification.json"));
            EnsembleContainerLayer<Vector2> dihedrals = EnsembleCalculations.dihedral_angles(PdbFun.getPdbFromId(id, TestsController.pdbPath, PdbFun.LoadingOptions.from_cache).Result);
            EnsembleFilter filter = new EnsembleFilter(chainId, modelId, residueSequenceNumber, residueType); //we can apply the filter before classification, bcs classification does not depend on any relations/neighboors
            return EnsembleCalculations.classify_ensemble(filter.filter(dihedrals), classifier);
        }

        /// <summary>
        /// Detailed Ramachandran classification for the protein backbone dihedral angles with advanced filtering based on Morris et al 1992. [DOI: 10.1002/prot.340120407]
        /// </summary>
        /// <remarks>
        /// Ramachandran classification for the protein backbone dihedral angles based on Morris et al 1992. [DOI: 10.1002/prot.340120407]
        /// Returns a count for core, allowed, generous and outside residues grouped at varius "levels":
        /// a total sum, per model, per chain, per residue (the last one only contains a single '1' value)
        /// Please note that the first and last residue are removed (due to missing psi/phi angle).
        /// GLY and PRO residues are also removed (due to strong type dependence).
        /// </remarks>
        /// <param name="id">PDB id</param>
        /// <param name="filter">filter structure</param>
        /// Note that this overrides the default of leaving out GLY and PRO residues. 
        /// Leave it empty (or use null) for a sum of all residue types</param>
        /// <returns>count of residues in the regions: Core, Allowed, Generous and Outside</returns>
        [HttpPost("{id}")]
        public EnsembleDto<RegionCounts> Classify(string id, [FromBody] EnsembleFilter filter)
        {
            //RamachandranClassifier_Morris1992 classifier = new RamachandranClassifier_Morris1992();
            //classifier.load_config(Path.Combine(henv.WebRootPath, "ramachandran_classification.json"));
            EnsembleContainerLayer<Vector2> dihedrals = EnsembleCalculations.dihedral_angles(PdbFun.getPdbFromId(id, TestsController.pdbPath, PdbFun.LoadingOptions.from_cache).Result);
            return EnsembleCalculations.classify_ensemble(filter.filter(dihedrals), classifier);
        }
    }
}
