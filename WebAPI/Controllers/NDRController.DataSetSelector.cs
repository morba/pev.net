﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using PEV.Calculations.Ting2010;

namespace WebAPI.Controllers
{
    public partial class NDRController : Controller
    {
        /// <summary>
        /// dataset enum for validation
        /// </summary>


        [Flags]
        public enum NDRDDataSet : long
        {
            LeftNeighboor = 1 << 3,
            RightNeighboor = 1 << 4,
            TCBIG = 1 << 11,
            TCB = 1 << 12,
            T = 1 << 13,
            C = 1 << 14,
            //combination
            NDRD_L_TCBIG = LeftNeighboor | TCBIG,
            NDRD_R_TCBIG = RightNeighboor | TCBIG,
            NDRD_R_TCB = RightNeighboor | TCB,
            NDRD_L_TCB = LeftNeighboor | TCB,
            NDRD_L_T = LeftNeighboor | T,
            NDRD_R_T = RightNeighboor | T,
            NDRD_R_C = RightNeighboor | C,
            NDRD_L_C = LeftNeighboor | C,
        }



        public static Dictionary<NDRDDataSet, NDRD> configs_NDRD = new Dictionary<NDRDDataSet, NDRD>();
        // Enum.GetValues(typeof(NDRDDataSet))
        //.Cast<long>().Where(x => !PEV.CommrutLib.isFlag(x)).Cast<NDRDDataSet>()
        //.ToDictionary(x => x, x => NDRD.fromJson(Path.Combine(TestsController.basePath, "NDRD", $"{x.ToString()}.json")));

        public static NDRDDataSet get_dataset_id(
            string dataset = nameof(NDRDDataSet.TCBIG),
            bool right_neighboor = false)
        {
            return (Enum.TryParse(dataset, out NDRDDataSet x) ? x : throw new ArgumentOutOfRangeException(nameof(dataset)))
                    | (right_neighboor ? NDRDDataSet.RightNeighboor : NDRDDataSet.LeftNeighboor);
        }

        public static NDRD get_config(NDRDDataSet id)
        {
            if (!configs_NDRD.ContainsKey(id))
            {
                configs_NDRD[id] = NDRD.fromJson(Path.Combine(TestsController.basePath, "NDRD", $"{id.ToString()}.json"));
            }
            return configs_NDRD[id];
        }

        public static NDRD get_config(string dataset = nameof(NDRDDataSet.TCBIG),
            bool right_neighboor = false) => get_config(get_dataset_id(dataset, right_neighboor));

    }
}
