﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Numerics;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Hosting;

using PEV.Models;
using PEV.Calculations;
using PEV.Calculations.Morris1992;
using PEV.Models.Ensemble;
using PEV.Models.Pdb;

namespace WebAPI.Controllers
{
    /// <summary>
    /// Attempt to make a Functional Programming inspired interface to the same library functions used in the other controllers
    /// </summary>
    [Route("[controller]/[action]")]
    public class FuncTest : Controller
    {


    }
}
