﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Hosting;

using PEV.Models;
using PEV.Calculations;
using System.Net.Http;
using Microsoft.Extensions.PlatformAbstractions;
using PEV.Models.Pdb;
using PEV.Models.Ensemble;
using System.Numerics;

namespace WebAPI.Controllers
{
    /// <summary>
    /// Tests to see if the application is working correctly
    /// </summary>
    [Route("[controller]/[action]")]
    public class TestsController : Controller
    {
        /// <summary>
        /// application root path
        /// </summary>
#if DEBUG
        public static readonly string basePath = 
            Environment.MachineName == "DESKTOP-UOV0L12" 
                ? @"F:\repos\PEV.NET\WebAPI\" 
                : Environment.MachineName == "DEV-I5" 
                    ? @"D:\repos\PEV.NET\WebAPI\"
                    : PlatformServices.Default.Application.ApplicationBasePath;
#else
        public static readonly string basePath = PlatformServices.Default.Application.ApplicationBasePath;
#endif

        /// <summary>
        /// path to PDB cache
        /// </summary>
        public static readonly string pdbPath = Path.Combine(basePath, "wwwroot", "PDB");

        //private IHostingEnvironment henv { get; }
        ///// <summary>
        ///// initialize the controller with the current hosting environment
        ///// </summary>
        ///// <param name="env"></param>
        //public TestsController(IHostingEnvironment env) => henv = env;

        /// <summary>
        /// Test if the server is running
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(200)]
        public StatusCodeResult Ping() => Ok();

        /// <summary>
        /// gets a PDB file from the local cache or th PDB server
        /// and returns the internal representation of this protein
        /// </summary>
        /// <param name="id">the PDB id</param>
        /// <param name="forceRefresh">force to download the PDB again</param>
        /// <returns>internal representation of this protein</returns>
        [HttpGet("{id}")]
        [ProducesResponseType(typeof(ProteinLayer), 200)]
        [ProducesResponseType(typeof(ProteinLayer), 404)]
        public IActionResult LoadPdb(string id = "1d3z", bool forceRefresh = false)
        {
            if (id.Length != 4)
            {
                return NotFound();
            }

            ProteinLayer p = PdbFun.getPdbFromId(id, pdbPath, forceRefresh ? PdbFun.LoadingOptions.force_refresh : PdbFun.LoadingOptions.save_gzip).Result;
            return Ok(p);
        }

        /// <summary>
        /// downloads and parses a pdb from database
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        [ProducesResponseType(200)]
        public StatusCodeResult FetchPDB(string id)
        {
            PdbFun.getPdbFromId(id, pdbPath, PdbFun.LoadingOptions.save_gzip).Wait();
            return Ok();
        }

        /// <summary>
        /// uploads a pdb file for later usage
        /// </summary>
        /// <remarks>
        /// You need to send the pdb file as text/plain in the body of the request. 
        /// You can later refer to the pdb you uloaded with the id you have given, if you are uploading a nonstandard / self made pdb file please give it a unique id.
        /// If a file with the same name already found then a 409 Conflict response is returned and no changes are being made.
        /// </remarks>
        /// <param name="id">name to be saved as (if you are uploading a self made pdb file plz use a unique name)</param>
        /// <returns></returns>
        [HttpPost("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(409)]
        public StatusCodeResult UploadPdb(string id)
        {
            if(System.IO.File.Exists(Path.Combine(pdbPath, id + ".pdb"))) return new StatusCodeResult(409);
            using(System.IO.FileStream fs = System.IO.File.Create(Path.Combine(pdbPath, id + ".pdb")))
            {
                Request.Body.CopyToAsync(fs).Wait();
            }
            return Ok();
        }


        /// <summary>
        /// gets PDB, calculate dihedral angles and stores it in a new structure. this new structure is returned for testing accuracy of dihedral angle calculations
        /// </summary>
        /// <param name="id">PDB id</param>
        /// <returns>an Ensemble representation of the protein</returns>
        [HttpGet("{id}")]
        public EnsembleDto<Vector2> CalculateDihedrals(string id) => 
            EnsembleCalculations.dihedral_angles(PdbFun.getPdbFromId(id, TestsController.pdbPath, PdbFun.LoadingOptions.from_cache).Result);
    }
}
