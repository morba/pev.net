﻿using System.IO;
using System.Linq;
using Xunit;
using PEV.Models.Pdb;
using PEV.Calculations;
using System.Collections.Generic;

namespace PEVLibTests
{
    public class DihedralAngles
    {
        [Theory]
        [InlineData("1d3z")] //simple
        [InlineData("1A03")] //multiple chains
        [InlineData("2N0T")] //simple
        [InlineData("2N0S", true, false)] //missing backbone atom
        [InlineData("1r9v")] //only hetatom
        [InlineData("2k39", false)] //normal
        [InlineData("9AAC-gromacs_full", false)] //generated1
        [InlineData("selected_16300_CA_N_H", false)] //generated2
        [InlineData("4pth", false)] //large
        [InlineData("2hyn", false)] //large and multi chain
        public void ThrowNoException(string pdb_id, bool online = true, bool noexception = true)
        {
            ProteinLayer readit = online ? PdbFun.getPdbFromId(pdb_id).Result : PdbFun.getPdbFromStream(File.OpenText(Path.Combine("../../../TestData/", pdb_id+".pdb")));
            if (noexception)
            {
                PEV.Models.Ensemble.EnsembleContainerLayer<System.Numerics.Vector2> result = EnsembleCalculations.dihedral_angles(readit);
                Assert.NotNull(result);
                Assert.Equal(readit.models.Count, result.childs.Count);
                Assert.Equal(readit.childs.Count(c => c.childs.Count > 0) * readit.models.Count, result.childs.Sum(m => m.childs.Count));
            }
            else
            {
                Assert.Throws<KeyNotFoundException>(() => EnsembleCalculations.dihedral_angles(readit));
            }
        }

    }

}
