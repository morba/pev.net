using System;
using System.IO;
using Xunit;
using PEV.Models;
using PEV.Models.Pdb;
using System.Linq;
using System.Numerics;

namespace PEVLibTests
{
    public class PdbParsing
    {
        [Theory]
        [InlineData("1d3z")] //simple
        [InlineData("1A03")] //multiple chains
        [InlineData("2N0T")] //simple
        [InlineData("2N0S")] //missing backbone atom
        [InlineData("1r9v")] //only hetatom
        public void NoExceptionsInDbLoading(string pdb_id)
        {
            ProteinLayer pdb = PdbFun.getPdbFromId(pdb_id).Result;
            Assert.NotNull(pdb);
        }

        [Theory]
        [InlineData("2k39.pdb")] //normal
        [InlineData("9AAC-gromacs_full.pdb")] //generated1
        [InlineData("selected_16300_CA_N_H.pdb")] //generated2
        [InlineData("4pth.pdb")] //large
        [InlineData("2hyn.pdb")] //large
        public void NoExceptionsInLocalLoading(string pdb_path)
        {
            ProteinLayer pdb = PdbFun.getPdbFromStream(File.OpenText(Path.Combine("../../../TestData/", pdb_path)));
            Assert.NotNull(pdb);
        }

        [Theory]
        [InlineData("2k39", 62, 'A', 45, "CD2", 20.961f, 21.313f, 26.947f, 1.00f, 0.00f)] //normal
        [InlineData("2k39", 93, 'A', 25, "HB3", 22.962f, 24.811f, 11.551f, 1.00f, 0.00f)]
        [InlineData("2k39", 33, 'A', 28, "N", 28.494f, 26.734f, 12.396f, 1.00f, 0.00f)]
        [InlineData("2hyn", 41, 'D', 23, "CA", 16.506f, -24.650f, -1.001f, 1.00f, 0.00f)] //multiple chains
        [InlineData("2hyn", 41, 'A', 12, "C", -7.033f, -0.486f, -18.117f, 1.00f, 0.00f)]
        [InlineData("2hyn", 41, 'B', 6, "O", 12.659f, 17.066f, -12.970f, 1.00f, 0.00f)]
        [InlineData("2hyn", 41, 'C', 49, "HG21", 0.569f, -21.402f, 26.454f, 1.00f, 0.00f)] //long atom name
        [InlineData("selected_16300_CA_N_H", 28, 'A', 72, "CA", 18.467f, 7.342f, -6.511f, 1.00f, 0.00f)] //generated
        [InlineData("selected_16300_CA_N_H", 9, 'A', 15, "N", 8.574f, 30.419f, -15.912f, 1.00f, 0.00f)] //generated
        public void RandomDataEquality(string pdb_path, int model, char chain, int residueSeqNum, string atomname, float x, float y, float z, float occupancy, float temp)
        {
            ProteinLayer pdb = PdbFun.getPdbFromStream(File.OpenText(Path.Combine("../../../TestData/", pdb_path+".pdb")));
            AtomData data_read = pdb.models.First(m=>m.me.pdbModelId == model).childs[pdb.childs.First(ch => ch.me.pdbChainId == chain).childs.First(res => res.me.sequenceNumber == residueSeqNum).atomMap[atomname].dataIndex];
            AtomData data_given = new AtomData()
            {
                occupancy = occupancy,
                tempFactor = temp,
                coordinates = new Vector3(x, y, z)
            };
            Assert.Equal(data_given, data_read);
        }
    }

}
