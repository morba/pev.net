﻿using System;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using PEV.Calculations;
using PEV.Models.Pdb;
using PEV.Calculations.Ting2010;
using PEV.Models.Ensemble;
using System.Collections.Generic;
using PEV.Calculations.Morris1992;

namespace PEV_NET
{
    class Program
    {
        public static readonly string drive = Environment.MachineName == "DEV-I5" ? "D:" : Environment.MachineName == "DESKTOP-UOV0L12" ? "F:" : "C:";

        public static readonly Classifier classifier
          = new Classifier(Path.Combine($@"{drive}\repos\PEV.NET\WebAPI\", "wwwroot", "ramachandran_classification.json"));

        public static readonly NDRD ndrd_l_tcbig = NDRD.fromJson($@"{drive}\repos\PEV.NET\WebAPI\NDRD\NDRD_L_TCBIG.json");

        public static readonly string working_dir = @"D:\pev\zolinak4\";

        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            //convert_RD();

            //compare_ramachandran_with_large_pdb(new string []{ "1d3z", "2k39", "2n0t", "5ivk" });
            //compare_ramachandran_with_large_pdb(File.ReadAllLines($@"{drive}\repos\PEV.NET\PEV.NET\7766_pdb.ids").Take(10000).ToArray(), new Uri("http://localhost:41111/api"));
            //compare_ramachandran_with_large_pdb(File.ReadAllLines(@"D:\repos\PEV.NET\PEV.NET\7766_pdb.ids").Take(10000).ToArray(), new Uri("http://pev-webapi.azurewebsites.net"));

            //double[] core_logprob = File.ReadAllLines(@"D:\repos\PEV.NET\PEV.NET\comparisons\aggregated1000.txt").Where(line => line.Length > 0 && line[0] != '#').Select(line => line.Split()).Where(row => row[2] == "C").Select(row => double.Parse(row[1])).ToArray();

            //test1();

            DateTime startt = DateTime.UtcNow;
            Console.WriteLine($"start at: {startt}");
            foreach (var ttype in new string[] { "TDRD", "NDRD" })
                foreach (var direction in new string[] { "R", "L" })
            foreach (var ds in new string[] { "TCBIG", "TCB", "T", "C" })
            {
                foreach (var aa in PdbFun.aa_list)
                {
                    using (FileStream fs = File.Create($@"{drive}\repos\PEV.NET\PEV.NET\bin\Release\{ttype}_{direction}_{ds}_{aa}_7766_stats.txt"))
                    using (StreamWriter sw = new StreamWriter(fs))
                    {
                        analyze_aggregated($@"{drive}\repos\PEV.NET\PEV.NET\bin\Release\{ttype}_{direction}_{ds}_aggregated_7766.txt", sw, aa);
                    }
                    DateTime endt = DateTime.UtcNow;
                    Console.WriteLine($"end at: {endt} total time:{endt - startt}");
                }
            }
            //File.WriteAllLines("2kg2_ramachandran.plot", EnsembleCalculations.ramachandran_plot(EnsembleCalculations.dihedral_angles(PdbFun.getPdbFromId("2kg2").Result)));


            //var t = new string[] {};
            //foreach(var q in t)
            //{
            //    //zolinak3(q.ToLower());
            //}

            //zolinak3("2kg2");

            Console.WriteLine("Hello World!");
        }

        //public static void test1()
        //{
        //    Protein p = Protein.fromPdbId("2n0t").Result;
        //    PEV.Calculations.Morris1992.Classifier mc = new Calculations.Morris1992.Classifier(@"D:\repos\PEV.NET\PEV.NET\ramachandran_classification.json");
        //    Models.Ensemble.EnsembleContainer<Calculations.Morris1992.RegionCounts> q = mc.classify(p, modelId: 1);
        //    foreach (Models.Ensemble.ResidueContainer<Calculations.Morris1992.RegionCounts> w in q.conformers[0].chains[0].residues.Where(r => r.data.outside > 0))
        //    {
        //        Console.WriteLine($"seqNum= {w.residue.sequenceNumber}");
        //    }
        //}

        public static void zolinak3(string orig_pdb_id)
        {
            var file_list = new string[] { orig_pdb_id }.Concat(Directory.EnumerateFiles(working_dir).Select(s => Path.GetFileName(s)).Where(s => s.StartsWith(orig_pdb_id) && s.EndsWith(".pdb")).Select(s => s.Substring(0, s.Length - 4))).ToArray();

            //var file_list = new string[] { "2kgx", "2kxg_all_100000_t1000-2000_bmrb", "2kxg_cocoplus_bmrb", "2kxg_unrest_fc200000_t1000-2000_bmrb", "2kxg_unviol_100000_t1000-2000_bmrb" };

            var pdb_ndrd_list = file_list.Select(fn => EnsembleCalculations.get_log_prob_for_ensemble(
                EnsembleCalculations.dihedral_angles(
                    PdbFun.getPdbFromId(fn, local_folder: working_dir, options: PdbFun.LoadingOptions.from_cache).Result), ndrd_l_tcbig
                )).ToArray();

            var pdb_morris_list = file_list.Select(fn => EnsembleCalculations.classify_ensemble(
                EnsembleCalculations.dihedral_angles(
                    PdbFun.getPdbFromId(fn, working_dir, options: PdbFun.LoadingOptions.from_cache).Result), classifier).transform_ensemble_data(x => (double)(x.core > 0 ? 3 : x.allowed > 0 ? 2 : x.generous > 0 ? 1 : x.outside > 0 ? 0 : -1))).ToArray();

            foreach (var pdb in pdb_ndrd_list)
            {
                EnsembleCalculations.add_ensemble_model(pdb, x => x.Average());
                var w1 = EnsembleCalculations.plot_data_by_model(pdb.transform_ensemble_data(x => x.ToString("G6", System.Globalization.CultureInfo.InvariantCulture)));
                var w2 = w1.Skip(1).Concat(w1.Take(1));
                var w3 = w2.Select(x => x.Split(' ')).ZipMany(x => x.Aggregate("", (acc, item) => acc + item + " "));
                File.WriteAllLines(Path.Combine(working_dir, $"{pdb.me.pdb.pdbId}#ndrd_models.txt"), w3);
            }

            foreach (var pdb in pdb_morris_list)
            {
                EnsembleCalculations.add_ensemble_model(pdb, x => x.Average());
                var w1 = EnsembleCalculations.plot_data_by_model(pdb.transform_ensemble_data(x => x.ToString("G6", System.Globalization.CultureInfo.InvariantCulture)));
                var w2 = w1.Skip(1).Concat(w1.Take(1));
                var w3 = w2.Select(x => x.Split(' ')).ZipMany(x => x.Aggregate("", (acc, item) => acc + item + " "));
                File.WriteAllLines(Path.Combine(working_dir, $"{pdb.me.pdb.pdbId}#morris_models.txt"), w3);
            }

            //EnsembleCalculations.add_ensemble_model(t, (x)=>x.OrderBy(y=>y).Percentile(0.5));
            var ndrd_aggregated = new EnsembleContainerLayer<double>
            {
                me = new EnsembleContainerDto<double>() { pdb = new Protein("temp", "") },
                childs = pdb_ndrd_list.Select(e => EnsembleCalculations.create_ensemble_model(e, (x) => x.Average())).ToList()
            };
            // cant add this comparison, bcs the pdbs have different chain id in 1 case
            //for (int i = 1; i < file_list.Length; i++)
            //{
            //    ndrd_aggregated.childs.Add(EnsembleCalculations.create_ensemble_model(ndrd_aggregated, (x) => x.First() - x.ElementAt(i)));
            //}

            //t = new EnsembleFilter() { modelIds = new int[] { int.MinValue } }.filter(t);
            var q1 = ndrd_aggregated.transform_ensemble_data(x => x.ToString("G6", System.Globalization.CultureInfo.InvariantCulture));
            //var first_line = new string[] { $"#{file_list.Aggregate("", (acc, item) => acc + item + " ")}{file_list.Skip(1).Aggregate("", (acc, item) => acc + $"{file_list[0]}---{item} ")} " };
            var first_line = new string[] { $"#{file_list.Aggregate("", (acc, item) => acc + item + " ")} " };
            var t1 = EnsembleCalculations.plot_data_by_model(q1);
            File.WriteAllLines(Path.Combine(working_dir, $"{file_list[0]}#agg_ndrd.txt"), first_line.Concat(t1.Skip(1)));


            //EnsembleCalculations.add_ensemble_model(t, (x)=>x.OrderBy(y=>y).Percentile(0.5));
            var morris_aggregated = new EnsembleContainerLayer<double>
            {
                me = new EnsembleContainerDto<double>() { pdb = new Protein("temp", "") },
                childs = pdb_morris_list.Select(e => EnsembleCalculations.create_ensemble_model(e, (x) => x.Average())).ToList()
            };
            // cant add this comparison, bcs the pdbs have different chain id in 1 case
            //for (int i = 1; i < file_list.Length; i++)
            //{
            //    morris_aggregated.childs.Add(EnsembleCalculations.create_ensemble_model(morris_aggregated, (x) => x.First() - x.ElementAt(i)));
            //}

            //t = new EnsembleFilter() { modelIds = new int[] { int.MinValue } }.filter(t);
            var q2 = morris_aggregated.transform_ensemble_data(x => x.ToString("G6", System.Globalization.CultureInfo.InvariantCulture));
            var t2 = EnsembleCalculations.plot_data_by_model(q2);
            File.WriteAllLines(Path.Combine(working_dir, $"{file_list[0]}#agg_morris.txt"), first_line.Concat(t2.Skip(1)));

        }


        public static void compare_ramachandran_with_large_pdb(string[] pdb_list, Uri base_uri)
        {
            int i = 0;
            int error_count = 0;
            DateTime startt = DateTime.UtcNow;
            Console.WriteLine($"start at: {startt}");
            using (FileStream fs = File.Create($"TDRD_R_T_aggregated_7766.txt"))
            using (StreamWriter sw = new StreamWriter(fs))
            {
                foreach (string id in pdb_list)
                {
                    i++;
                    Console.WriteLine($"processing {i} / {pdb_list.Length} pdb: {id} ");
                    try
                    {
                        sw.WriteLine($"#pdb: {id}");
                        sw.Flush();
                        save_web_response(new Uri(base_uri.AbsoluteUri + $"/Plot/PlotMorrisClassVsTinglogProb/{id}?neighboor_dependent=false&dataset=T&right_neighboor=true"), fs).Wait();
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine($"error in {i} / {pdb_list.Length} pdb: {id}");
                        error_count++;
                        File.WriteAllText($"error_{id}.txt", e.Message + "\n\n" + e.InnerException + "\n\n" + e.StackTrace + "\n\n" + e.InnerException?.StackTrace);
                    }
                }
            }
            DateTime endt = DateTime.UtcNow;
            Console.WriteLine($"end at: {endt} total time:{endt - startt}");
            Console.WriteLine($"errors: {error_count}");
        }

        public static async Task save_web_response(Uri uri, Stream output)
        {
            using (HttpClient webclient = new HttpClient())
            {
                await (await webclient.GetAsync(uri)).EnsureSuccessStatusCode().Content.CopyToAsync(output);
            }
        }

        public static void analyze_aggregated(string input_file, StreamWriter output, string aa_type = null)
        {
            double[] c = File.ReadAllLines(input_file)
                .Where(line => line.Length > 0 && line[0] != '#')
                .Select(line => line.Split())
                .Where(row => row[2] == "C" && row[0].ToUpper().EndsWith(aa_type?.ToUpper() ?? ""))
                .Select(row => double.Parse(row[1], System.Globalization.CultureInfo.InvariantCulture))
                .Where(x => !double.IsInfinity(x)).OrderBy(x => x).ToArray();
            double[] a = File.ReadAllLines(input_file)
                .Where(line => line.Length > 0 && line[0] != '#')
                .Select(line => line.Split())
                .Where(row => row[2] == "A" && row[0].ToUpper().EndsWith(aa_type?.ToUpper() ?? ""))
                .Select(row => double.Parse(row[1], System.Globalization.CultureInfo.InvariantCulture))
                .Where(x => !double.IsInfinity(x)).OrderBy(x => x).ToArray();
            double[] g = File.ReadAllLines(input_file)
                .Where(line => line.Length > 0 && line[0] != '#')
                .Select(line => line.Split())
                .Where(row => row[2] == "G" && row[0].ToUpper().EndsWith(aa_type?.ToUpper() ?? ""))
                .Select(row => double.Parse(row[1], System.Globalization.CultureInfo.InvariantCulture))
                .Where(x => !double.IsInfinity(x)).OrderBy(x => x).ToArray();
            double[] o = File.ReadAllLines(input_file)
                .Where(line => line.Length > 0 && line[0] != '#')
                .Select(line => line.Split())
                .Where(row => row[2] == "O" && row[0].ToUpper().EndsWith(aa_type?.ToUpper() ?? ""))
                .Select(row => double.Parse(row[1], System.Globalization.CultureInfo.InvariantCulture))
                .Where(x => !double.IsInfinity(x)).OrderBy(x => x).ToArray();

            //double[][] all = new double[][] { c, a, g, o };

            output.AutoFlush = true;
            output.WriteLine($"# Core Allowed Generous Outside");
            output.WriteLine($"count: {c.Length} {a.Length} {g.Length} {o.Length}");

            if (c.Length == 0 || a.Length == 0 || g.Length == 0 || o.Length == 0)
            {
                return;
            }

            output.WriteLine($"average: {c.Average()} {a.Average()} {g.Average()} {o.Average()}");
            output.WriteLine($"sample_std_deviation: {c.StdDev()} {a.StdDev()} {g.StdDev()} {o.StdDev()}");

            double resolution = 20.0;
            for (int i = 0; i <= resolution; i++)
            {
                output.WriteLine($"{(i * 100 / resolution)}%_percentile: {c.OrderBy(x => x).Percentile(i / resolution)} {a.OrderBy(x => x).Percentile(i / resolution)} {g.OrderBy(x => x).Percentile(i / resolution)} {o.OrderBy(x => x).Percentile(i / resolution)}");
            }
        }


        public static void convert_RD()
        {
            PEV.Calculations.Ting2010.TDRD t = new PEV.Calculations.Ting2010.TDRD();
            t.load_wasting_datafile($@"{drive}\repos\PEV.NET\PEV.NET\NDRD\NDRD_TCB.txt");
            File.WriteAllText($@"{drive}\repos\PEV.NET\PEV.NET\NDRD\TDRD_L_TCB.json", JsonConvert.SerializeObject(t));
            t.load_wasting_datafile($@"{drive}\repos\PEV.NET\PEV.NET\NDRD\NDRD_TCB.txt", "right");
            File.WriteAllText($@"{drive}\repos\PEV.NET\PEV.NET\NDRD\TDRD_R_TCB.json", JsonConvert.SerializeObject(t));

            PEV.Calculations.Ting2010.NDRD n = new PEV.Calculations.Ting2010.NDRD();
            n.load_wasting_datafile($@"{drive}\repos\PEV.NET\PEV.NET\NDRD\NDRD_TCB.txt");
            File.WriteAllText($@"{drive}\repos\PEV.NET\PEV.NET\NDRD\NDRD_L_TCB.json", JsonConvert.SerializeObject(n));
            n.load_wasting_datafile($@"{drive}\repos\PEV.NET\PEV.NET\NDRD\NDRD_TCB.txt", "right");
            File.WriteAllText($@"{drive}\repos\PEV.NET\PEV.NET\NDRD\NDRD_R_TCB.json", JsonConvert.SerializeObject(n));

            PEV.Calculations.Ting2010.TDRD t1 = new PEV.Calculations.Ting2010.TDRD();
            t.load_wasting_datafile($@"{drive}\repos\PEV.NET\PEV.NET\NDRD\NDRD_TCBIG.txt");
            File.WriteAllText($@"{drive}\repos\PEV.NET\PEV.NET\NDRD\TDRD_L_TCBIG.json", JsonConvert.SerializeObject(t));
            t.load_wasting_datafile($@"{drive}\repos\PEV.NET\PEV.NET\NDRD\NDRD_TCBIG.txt", "right");
            File.WriteAllText($@"{drive}\repos\PEV.NET\PEV.NET\NDRD\TDRD_R_TCBIG.json", JsonConvert.SerializeObject(t));

            PEV.Calculations.Ting2010.NDRD n1 = new PEV.Calculations.Ting2010.NDRD();
            n.load_wasting_datafile($@"{drive}\repos\PEV.NET\PEV.NET\NDRD\NDRD_TCBIG.txt");
            File.WriteAllText($@"{drive}\repos\PEV.NET\PEV.NET\NDRD\NDRD_L_TCBIG.json", JsonConvert.SerializeObject(n));
            n.load_wasting_datafile($@"{drive}\repos\PEV.NET\PEV.NET\NDRD\NDRD_TCBIG.txt", "right");
            File.WriteAllText($@"{drive}\repos\PEV.NET\PEV.NET\NDRD\NDRD_R_TCBIG.json", JsonConvert.SerializeObject(n));

            PEV.Calculations.Ting2010.TDRD t2 = new PEV.Calculations.Ting2010.TDRD();
            t.load_wasting_datafile($@"{drive}\repos\PEV.NET\PEV.NET\NDRD\NDRD_Tonly.txt");
            File.WriteAllText($@"{drive}\repos\PEV.NET\PEV.NET\NDRD\TDRD_L_T.json", JsonConvert.SerializeObject(t));
            t.load_wasting_datafile($@"{drive}\repos\PEV.NET\PEV.NET\NDRD\NDRD_Tonly.txt", "right");
            File.WriteAllText($@"{drive}\repos\PEV.NET\PEV.NET\NDRD\TDRD_R_T.json", JsonConvert.SerializeObject(t));

            PEV.Calculations.Ting2010.NDRD n2 = new PEV.Calculations.Ting2010.NDRD();
            n.load_wasting_datafile($@"{drive}\repos\PEV.NET\PEV.NET\NDRD\NDRD_Tonly.txt");
            File.WriteAllText($@"{drive}\repos\PEV.NET\PEV.NET\NDRD\NDRD_L_T.json", JsonConvert.SerializeObject(n));
            n.load_wasting_datafile($@"{drive}\repos\PEV.NET\PEV.NET\NDRD\NDRD_Tonly.txt", "right");
            File.WriteAllText($@"{drive}\repos\PEV.NET\PEV.NET\NDRD\NDRD_R_T.json", JsonConvert.SerializeObject(n));

            PEV.Calculations.Ting2010.TDRD t3 = new PEV.Calculations.Ting2010.TDRD();
            t.load_wasting_datafile($@"{drive}\repos\PEV.NET\PEV.NET\NDRD\NDRD_Conly.txt");
            File.WriteAllText($@"{drive}\repos\PEV.NET\PEV.NET\NDRD\TDRD_L_C.json", JsonConvert.SerializeObject(t));
            t.load_wasting_datafile($@"{drive}\repos\PEV.NET\PEV.NET\NDRD\NDRD_Conly.txt", "right");
            File.WriteAllText($@"{drive}\repos\PEV.NET\PEV.NET\NDRD\TDRD_R_C.json", JsonConvert.SerializeObject(t));

            PEV.Calculations.Ting2010.NDRD n3 = new PEV.Calculations.Ting2010.NDRD();
            n.load_wasting_datafile($@"{drive}\repos\PEV.NET\PEV.NET\NDRD\NDRD_Conly.txt");
            File.WriteAllText($@"{drive}\repos\PEV.NET\PEV.NET\NDRD\NDRD_L_C.json", JsonConvert.SerializeObject(n));
            n.load_wasting_datafile($@"{drive}\repos\PEV.NET\PEV.NET\NDRD\NDRD_Conly.txt", "right");
            File.WriteAllText($@"{drive}\repos\PEV.NET\PEV.NET\NDRD\NDRD_R_C.json", JsonConvert.SerializeObject(n));
        }



    }
}